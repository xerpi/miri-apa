#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "prefetcher.h"
#include "isb_lite.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

typedef struct {
	uint64_t pc;
	uint64_t addr_line;
} mem_access_t;

static void print_ctx(const amc_ctx_t *ctx)
{
	printf("Training Unit:\n");
	training_unit_print(&ctx->training_unit);

	printf("PS-AMC: [tag, sa, counter, valid]\n");
	ps_amc_print(&ctx->ps);

	printf("SP-AMC: [tag, pa_line, valid]\n");
	sp_amc_print(&ctx->sp);
}

int main(int argc, char *argv[])
{
	printf("Training Unit %d, PS SP %d ways, %d sets\n", TRAINING_UNIT_NUM_STREAMS, PS_AMC_NUM_WAYS, PS_AMC_NUM_SETS);

	amc_ctx_t ctx;
	amc_ctx_reset(&ctx);

	printf("AMC context size: %d bits = %f KB\n", AMC_CTX_SIZE, (AMC_CTX_SIZE / 8) / 1024.0);

	static const mem_access_t accesses[] = {
		{0x1000, 0xAA},
		{0x1000, 0xBB},
		{0x1000, 0XCC},
		{0x1000, 0XAA},
	};

	for (int i = 0; i < ARRAY_SIZE(accesses); i++) {
		/* Perform training */
		amc_ctx_train(&ctx, accesses[i].pc, accesses[i].addr_line);

		/* Print data structures */
		print_ctx(&ctx);
	}

	uint64_t prediction[4];
	int num_predicted = amc_ctx_predict(&ctx, 0xaa, prediction, ARRAY_SIZE(prediction));
	printf("Num predicted: %d\n", num_predicted);
	for (int i = 0; i < num_predicted; i++)
		printf("  0x%lx\n", prediction[i]);

	return 0;
}
