#ifndef PENDING_PREFETCH_QUEUE_H
#define PENDING_PREFETCH_QUEUE_H

#include <stdint.h>
#include <stdbool.h>
#include "isb_lite.h"

/*
 * Physical address:
 * ___________________________________________________________________
 * |                       Page                    |   Page offset   |
 * -------------------------------------------------------------------
 * The prefetch queue is direct-mapped and stores {page -> page line}:
 * ___________________________________________________________________
 * |                  Tag                |  Entry  |  Line  | Offset |
 * -------------------------------------------------------------------
 *
 * We only need to save the cache line index within the page offset.
 */

#define PENDING_PREFETCH_QUEUE_ENTRY_NUM	128
#define PENDING_PREFETCH_ENTRY_OFFSET		0
#define PENDING_PREFETCH_ENTRY_BITS       BITS_TO_REPRESENT(PENDING_PREFETCH_QUEUE_ENTRY_NUM - 1)
#define PENDING_PREFETCH_ENTRY_MASK       MASK(PENDING_PREFETCH_ENTRY_BITS)
#define PENDING_PREFETCH_TAG_OFFSET       PENDING_PREFETCH_ENTRY_BITS
#define PENDING_PREFETCH_TAG_BITS         (ADDR_BITS - PAGE_OFFSET_BITS - PENDING_PREFETCH_ENTRY_BITS)
#define PENDING_PREFETCH_TAG_MASK         MASK(PENDING_PREFETCH_TAG_BITS)

typedef struct {
	uint64_t tag;  /* Tag of the page */
	uint64_t line; /* Line within the page */
	bool     valid;
} pending_prefetch_entry_t;

#define PENDING_PREFETCH_ENTRY_SIZE	(ADDR_BITS - CACHE_LINE_OFFSET_BITS + 1)

typedef struct {
	pending_prefetch_entry_t entry[PENDING_PREFETCH_QUEUE_ENTRY_NUM];
} pending_prefetch_queue_t;

#define PENDING_PREFETCH_QUEUE_SIZE	(PENDING_PREFETCH_ENTRY_SIZE * PENDING_PREFETCH_QUEUE_ENTRY_NUM)

void pending_prefetch_queue_reset(pending_prefetch_queue_t *queue);
void pending_prefetch_queue_map(pending_prefetch_queue_t *queue, uint64_t page, uint64_t line);
pending_prefetch_entry_t *pending_prefetch_queue_query(pending_prefetch_queue_t *queue, uint64_t page);
void pending_prefetch_queue_cache_fill(pending_prefetch_queue_t *queue, uint64_t addr_page);
void pending_prefetch_queue_print(const pending_prefetch_queue_t *queue);

#endif
