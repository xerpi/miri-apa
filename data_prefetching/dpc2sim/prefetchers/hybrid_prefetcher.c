//
// Data Prefetching Championship Simulator 2
// Seth Pugsley, seth.h.pugsley@intel.com
//

/*

  This file does NOT implement any prefetcher, and is just an outline

 */

#include <stdio.h>
#include "../inc/prefetcher.h"
#include "ampm_lite_cut.h"
#include "isb_lite.h"
#include "pending_prefetch_queue.h"

#define ISB_NUM_PREDICTION	8
#define ISB_MSHR_THRESHOLD	8
#define AMPM_MSHR_THRESHOLD	8
#ifndef DENSITY_THRESHOLD
	#define DENSITY_THRESHOLD	15 // Out of 100
#endif

#define HYBRID_SIZE		(AMC_CTX_SIZE + AMPM_SIZE + PENDING_PREFETCH_QUEUE_SIZE)

static pending_prefetch_queue_t pending_prefetch_isb_queue;

static amc_ctx_t amc_ctx;
static ampm_page_t ampm_pages[AMPM_PAGE_COUNT];

static inline int trigger_prefetch(uint64_t base_addr, uint64_t pf_addr, int mshr_threshold)
{
	/* Prefetch to L2 or LLC depending on the MSHR occupancy */
	if (get_l2_mshr_occupancy(0) < mshr_threshold)
		return l2_prefetch_line(0, base_addr, pf_addr, FILL_L2);
	else
		return l2_prefetch_line(0, base_addr, pf_addr, FILL_LLC);
}

void trigger_prefetch_queue(pending_prefetch_queue_t *queue, uint64_t base_addr, uint64_t addr_page)
{
	pending_prefetch_entry_t * pending = pending_prefetch_queue_query(queue, addr_page);
	if (pending) {
		/* Reconstruct the address to prefetch from */
		uint64_t pf_addr = (addr_page << PAGE_OFFSET_BITS) |
		                   (pending->line << CACHE_LINE_OFFSET_BITS);
		int ret = trigger_prefetch(base_addr, pf_addr, 8);

		/* If the prefetch was successful, remove it from the pending queue */
		if (ret)
			pending->valid = false;
	}
}

void isb_prefetching(amc_ctx_t *amc_ctx, uint64_t pc, uint64_t addr, bool allow_direct_prefetch)
{
	int num_predicted;
	uint64_t prediction[ISB_NUM_PREDICTION];
	pending_prefetch_entry_t *pending;
	uint64_t addr_line = addr >> CACHE_LINE_OFFSET_BITS;
	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	// ISB Prediction
	num_predicted = amc_ctx_predict(amc_ctx, addr_line, prediction, ISB_NUM_PREDICTION);

	/* For each predicted address to prefetch */
	for (int i = 0; i < num_predicted; i++) {
		uint64_t pf_page = prediction[i] >> PAGE_LINE_BITS;

		/* If the predicted page is the same as the one we are operating, we can issue the prefetch */
		if (allow_direct_prefetch && (pf_page == addr_page)) {
			/* Reconstruct the address to prefetch from */
			uint64_t pf_addr = prediction[i] << CACHE_LINE_OFFSET_BITS;

			/* Prefetch with MSHR threshold */
			trigger_prefetch(addr, pf_addr, ISB_MSHR_THRESHOLD);
		} else {
			/* Otherwise, We add the prefetch to the pending queue, while
			 * only allowing 1 pending prefetch per page at max. */
			pending = pending_prefetch_queue_query(&pending_prefetch_isb_queue, pf_page);
			if (!pending) {
				pending_prefetch_queue_map(&pending_prefetch_isb_queue, pf_page,
							   prediction[i] & PAGE_LINE_MASK);
			}
		}

	}

	// ISB Training
	amc_ctx_train(amc_ctx, pc, addr_line);
}

void l2_prefetcher_initialize(int cpu_num)
{
	printf("Hybrid Prefetcher (AMPM + ISB), size: %d bits = %f KB\n", HYBRID_SIZE, (HYBRID_SIZE / 8) / 1024.0);
	// you can inspect these knob values from your code to see which configuration you're runnig in
	printf("Knobs visible from prefetcher: %d %d %d\n", knob_scramble_loads, knob_small_llc, knob_low_bandwidth);

	amc_ctx_reset(&amc_ctx);
	ampm_initialize(ampm_pages);
	pending_prefetch_queue_reset(&pending_prefetch_isb_queue);
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr, unsigned long long int ip, int cache_hit)
{
	// uncomment this line to see all the information available to make prefetch decisions
	//printf("(0x%llx 0x%llx %d %d %d) ", addr, ip, cache_hit, get_l2_read_queue_occupancy(0), get_l2_mshr_occupancy(0));

	uint64_t ampm_prediction[2 * AMPM_MAX_ADDRESS_RANGE];
	int ampm_num_predicted;
	int ampm_page_index;
	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	/* Get AMPM page index */
	ampm_page_index = ampm_get_page_index(ampm_pages, addr);

	/* Select the prefetcher based on the "Access Map Density" */
	uint64_t density = ampm_get_density(ampm_pages, ampm_page_index);
	bool use_ampm = (density > DENSITY_THRESHOLD); //Accesses are regular, use AMPM

	/* Pending prefetch queue */
	if (!use_ampm)
		trigger_prefetch_queue(&pending_prefetch_isb_queue, addr, addr_page);

	/* AMPM Prefetching */
	ampm_prefetching(ampm_pages, ampm_page_index, addr,
		ampm_prediction, &ampm_num_predicted);

	/* ISB Prefetching */
	isb_prefetching(&amc_ctx, ip, addr, !use_ampm);

	/* For each AMPM predicted prefetch */
	for (int i = 0; i < ampm_num_predicted; i++)
		trigger_prefetch(addr, ampm_prediction[i], AMPM_MSHR_THRESHOLD);
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, int prefetch, unsigned long long int evicted_addr)
{
	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	pending_prefetch_queue_cache_fill(&pending_prefetch_isb_queue, addr_page);
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
	printf("Prefetcher heartbeat stats\n");
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
	printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
	printf("Prefetcher final stats\n");
}
