//
// Data Prefetching Championship Simulator 2
// Seth Pugsley, seth.h.pugsley@intel.com
//

/*

  This file describes a prefetcher that resembles a simplified version of the
  Access Map Pattern Matching (AMPM) prefetcher, which won the first
  Data Prefetching Championship.  The original AMPM prefetcher tracked large
  regions of virtual address space to make prefetching decisions, but this
  version works only on smaller 4 KB physical pages.

 */

#include <stdio.h>
#include "../inc/prefetcher.h"
#include "ampm_lite_cut.h"

void ampm_initialize(ampm_page_t * ampm_pages)
{
	for (int i = 0; i < AMPM_PAGE_COUNT; i++)
	{
		ampm_pages[i].page = 0;
		ampm_pages[i].lru = 0;
		for (int j = 0; j < AMPM_ACCESS_MAP; j++)
		{
			ampm_pages[i].access_map[j] = 0;
			ampm_pages[i].pf_map[j] = 0;
		}
	}
}

int ampm_is_page_hit(ampm_page_t * ampm_pages, uint64_t page)
{
	for (int i = 0; i < AMPM_PAGE_COUNT; i++) {
		if (ampm_pages[i].page == page)
			return i;
	}

	return -1;
}

int ampm_find_oldest_page(ampm_page_t * ampm_pages)
{
	int page_index = -1;
	int lru_index = 0;
	unsigned long long int lru_cycle = ampm_pages[lru_index].lru;

	for(int i = 0; i < AMPM_PAGE_COUNT; i++) {
		if (ampm_pages[i].lru < lru_cycle) {
			lru_index = i;
			lru_cycle = ampm_pages[lru_index].lru;
		}
	}
	page_index = lru_index;

	return page_index;
}

void ampm_reset_page(ampm_page_t * ampm_pages, int page_index, uint64_t page)
{
   if (page_index >= 0 && page_index <= AMPM_PAGE_COUNT)
	ampm_pages[page_index].page = page;

   for(int i = 0; i < AMPM_ACCESS_MAP; i++)
   {
      ampm_pages[page_index].access_map[i] = 0;
      ampm_pages[page_index].pf_map[i] = 0;
   }
}

uint64_t ampm_get_density(ampm_page_t * ampm_pages, int page_index)
{
	uint64_t count = 0;

	for(int i = 0; i < AMPM_ACCESS_MAP; i++) {
		count += ampm_pages[page_index].access_map[i] > 0 ? 1 : 0;
	}

	return (100 * count) / AMPM_ACCESS_MAP;
}

static void ampm_positive_prefetching(ampm_page_t * ampm_pages, int page_index, uint64_t page,
	uint64_t page_offset, uint64_t addr, uint64_t prediction[AMPM_MAX_ADDRESS_RANGE], int *num_predicted)
{
	int count_prefetches = 0;
	for (int i = 1; i <= AMPM_MAX_ADDRESS_RANGE; i++) {
		int check_index1 = page_offset - i;
		int check_index2 = page_offset - 2*i;
		int pf_index = page_offset + i;

		if (check_index2 < 0)
			break;

		if (pf_index > AMPM_ACCESS_MAP - 1)
			break;

		if (count_prefetches >= AMPM_PREFETCH_DEGREE)
			break;

		if (ampm_pages[page_index].access_map[pf_index] == 1) {
			// don't prefetch something that's already been demand accessed
			continue;
		}

		if (ampm_pages[page_index].pf_map[pf_index] == 1) {
			// don't prefetch something that's alrady been prefetched
			continue;
		}

		if ((ampm_pages[page_index].access_map[check_index1]==1) && (ampm_pages[page_index].access_map[check_index2]==1)) {
			// we found the stride repeated twice, so issue a prefetch
			unsigned long long int pf_address = (page<<12)+(pf_index<<6);
			// Add address to be prefetched to the prediction list
			prediction[count_prefetches] = pf_address;

			// mark the prefetched line so we don't prefetch it again
			ampm_pages[page_index].pf_map[pf_index] = 1;
			count_prefetches++;
		}
	}

	*num_predicted = count_prefetches;
}

static void ampm_negative_prefetching(ampm_page_t * ampm_pages, int page_index, uint64_t page,
	uint64_t page_offset, uint64_t addr, uint64_t prediction[AMPM_MAX_ADDRESS_RANGE], int *num_predicted)
{
	int count_prefetches = 0;
	for (int i = 1; i <= AMPM_MAX_ADDRESS_RANGE; i++) {
		int check_index1 = page_offset + i;
		int check_index2 = page_offset + 2*i;
		int pf_index = page_offset - i;

		if (check_index2 > AMPM_ACCESS_MAP - 1)
			break;

		if (pf_index < 0)
			break;

		if (count_prefetches >= AMPM_PREFETCH_DEGREE)
			break;

		if (ampm_pages[page_index].access_map[pf_index] == 1) {
			// don't prefetch something that's already been demand accessed
			continue;
		}

		if (ampm_pages[page_index].pf_map[pf_index] == 1) {
			// don't prefetch something that's alrady been prefetched
			continue;
		}

		if ((ampm_pages[page_index].access_map[check_index1]==1) && (ampm_pages[page_index].access_map[check_index2]==1)) {
			// we found the stride repeated twice, so issue a prefetch
			unsigned long long int pf_address = (page<<12)+(pf_index<<6);
			// Add address to be prefetched to the AMPM prediction list
			prediction[count_prefetches] = pf_address;
			// mark the prefetched line so we don't prefetch it again
			ampm_pages[page_index].pf_map[pf_index] = 1;
			count_prefetches++;
		}
	}

	*num_predicted = count_prefetches;
}

int ampm_get_page_index(ampm_page_t *ampm_pages, uint64_t addr)
{
	unsigned long long int cl_address = addr>>6;
	unsigned long long int page = cl_address>>6;
	int page_index = ampm_is_page_hit(ampm_pages, page);

	// If page not found, replace old one
	if (page_index == -1) {
		page_index = ampm_find_oldest_page(ampm_pages);
		ampm_reset_page(ampm_pages, page_index, page);
	}

	return page_index;
}

int ampm_prefetching(ampm_page_t *ampm_pages, int page_index, uint64_t addr,
	uint64_t prediction[2 * AMPM_MAX_ADDRESS_RANGE], int *num_predicted)
{
	int num_predicted_pos;
	int num_predicted_neg;
	unsigned long long int cl_address = addr>>6;
	unsigned long long int page = cl_address>>6;
	unsigned long long int page_offset = cl_address&63;

	// update LRU
	ampm_pages[page_index].lru = get_current_cycle(0);
	// mark the access map
	ampm_pages[page_index].access_map[page_offset] = 1;

	ampm_positive_prefetching(ampm_pages, page_index, page, page_offset,
		addr, &prediction[0], &num_predicted_pos);

	ampm_negative_prefetching(ampm_pages, page_index, page, page_offset,
		addr, &prediction[num_predicted_pos], &num_predicted_neg);

	*num_predicted = num_predicted_pos + num_predicted_neg;

	return page_index;
}
