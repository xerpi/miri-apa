#ifndef ISB_LITE_H
#define ISB_LITE_H

#include <stdint.h>
#include <stdbool.h>
#include "prefetcher.h"

/* https://stackoverflow.com/a/39920811 */
#define NEEDS_BIT(N, B)      (((unsigned long)(N) >> (B)) > 0)
#define BITS_TO_REPRESENT(N)                   \
	(NEEDS_BIT(N,  0) + NEEDS_BIT(N,  1) + \
	 NEEDS_BIT(N,  2) + NEEDS_BIT(N,  3) + \
	 NEEDS_BIT(N,  4) + NEEDS_BIT(N,  5) + \
	 NEEDS_BIT(N,  6) + NEEDS_BIT(N,  7) + \
	 NEEDS_BIT(N,  8) + NEEDS_BIT(N,  9) + \
	 NEEDS_BIT(N, 10) + NEEDS_BIT(N, 11) + \
	 NEEDS_BIT(N, 12) + NEEDS_BIT(N, 13) + \
	 NEEDS_BIT(N, 14) + NEEDS_BIT(N, 15) + \
	 NEEDS_BIT(N, 16) + NEEDS_BIT(N, 17) + \
	 NEEDS_BIT(N, 18) + NEEDS_BIT(N, 19) + \
	 NEEDS_BIT(N, 20) + NEEDS_BIT(N, 21) + \
	 NEEDS_BIT(N, 22) + NEEDS_BIT(N, 23) + \
	 NEEDS_BIT(N, 24) + NEEDS_BIT(N, 25) + \
	 NEEDS_BIT(N, 26) + NEEDS_BIT(N, 27) + \
	 NEEDS_BIT(N, 28) + NEEDS_BIT(N, 29) + \
	 NEEDS_BIT(N, 30) + NEEDS_BIT(N, 31)   \
	)

#define MASK(n)			((1ULL << (n)) - 1)

#define ADDR_BITS		64
#define STRUCTURAL_ADDR_BITS	64
#define CACHE_LINE_OFFSET_BITS	BITS_TO_REPRESENT(CACHE_LINE_SIZE - 1)
#define PAGE_OFFSET_BITS	BITS_TO_REPRESENT(PAGE_SIZE - 1)
#define ADDR_CACHE_LINE_BITS	(ADDR_BITS - CACHE_LINE_OFFSET_BITS)
#define PAGE_LINE_BITS		(PAGE_OFFSET_BITS - CACHE_LINE_OFFSET_BITS)
#define PAGE_LINE_MASK		MASK(PAGE_LINE_BITS)

/*
 * Physical address (line) mapping to PS-AMC (set-associative):
 * ____________________________________________              _________
 * |            TAG             |     Set     |, Discarded: | Offset |
 * --------------------------------------------              ---------
 *
 *
 * Structural address mapping to SP-AMC (set-associative):
 * _____________________________________________________
 * |                TAG                 |     Set      |
 * -----------------------------------------------------
 */
#ifdef WAYS
	#define PS_AMC_NUM_WAYS		WAYS
#else
	#define PS_AMC_NUM_WAYS		16
#endif
#define PS_AMC_NUM_WAYS_BITS	BITS_TO_REPRESENT(PS_AMC_NUM_WAYS - 1)
#ifdef SETS
	#define PS_AMC_NUM_SETS		SETS
#else
	#define PS_AMC_NUM_SETS		32
#endif
#define PS_AMC_SET_OFFSET	0
#define PS_AMC_SET_BITS		BITS_TO_REPRESENT(PS_AMC_NUM_SETS - 1)
#define PS_AMC_SET_MASK		MASK(PS_AMC_SET_BITS)
#define PS_AMC_TAG_OFFSET	(PS_AMC_SET_OFFSET + PS_AMC_SET_BITS)
#define PS_AMC_TAG_BITS		(ADDR_BITS - PS_AMC_TAG_OFFSET)
#define PS_AMC_TAG_MASK	 	MASK(PS_AMC_TAG_BITS)

#define PS_AMC_CONF_CNTR_BITS	2
#define PS_AMC_CONF_CNTR_MAX	((1U << PS_AMC_CONF_CNTR_BITS) - 1)

#ifdef WAYS
	#define SP_AMC_NUM_WAYS		WAYS
#else
	#define SP_AMC_NUM_WAYS		16
#endif
#define SP_AMC_NUM_WAYS_BITS	BITS_TO_REPRESENT(SP_AMC_NUM_WAYS - 1)
#ifdef SETS
	#define SP_AMC_NUM_SETS		SETS
#else
	#define SP_AMC_NUM_SETS		32
#endif
#define SP_AMC_SET_OFFSET 	0
#define SP_AMC_SET_BITS	 	BITS_TO_REPRESENT(SP_AMC_NUM_SETS - 1)
#define SP_AMC_SET_MASK	 	MASK(SP_AMC_SET_BITS)
#define SP_AMC_TAG_OFFSET	(SP_AMC_SET_OFFSET + SP_AMC_SET_BITS)
#define SP_AMC_TAG_BITS		(STRUCTURAL_ADDR_BITS - SP_AMC_SET_BITS)
#define SP_AMC_TAG_MASK	 	MASK(SP_AMC_TAG_BITS)

#ifdef TRAINING
	#define TRAINING_UNIT_NUM_STREAMS	TRAINING
#else
	#define TRAINING_UNIT_NUM_STREAMS	256
#endif
#define TRAINING_UNIT_NUM_STREAMS_BITS	BITS_TO_REPRESENT(TRAINING_UNIT_NUM_STREAMS - 1)

#define AMC_SA_CHUNK_SIZE	4096

/** PS-AMC structures **/

// Entry of the table PS-AMC (to translate from physical address to structural address)
typedef struct {
	uint64_t tag;     /* Tag of the Physical address */
	uint64_t sa;      /* Structural address */
	uint8_t  counter; /* Confidence counter */
	bool     valid;	  /* Whether this entry is valid or not */
} ps_amc_entry_t;

#define PS_AMC_ENTRY_SIZE (PS_AMC_TAG_BITS + STRUCTURAL_ADDR_BITS + PS_AMC_CONF_CNTR_BITS + 1)

// Set of PS-AMC entries (ways)
typedef struct {
	ps_amc_entry_t way[PS_AMC_NUM_WAYS];
	int            lru[PS_AMC_NUM_WAYS];
} ps_amc_set_t;

#define PS_AMC_SET_SIZE ((PS_AMC_ENTRY_SIZE + PS_AMC_NUM_WAYS_BITS) * PS_AMC_NUM_WAYS)

// Aggregation of PS-AMC sets. Set-associative cache.
typedef struct {
	ps_amc_set_t set[PS_AMC_NUM_SETS];
} ps_amc_t;

#define PS_AMC_SIZE (PS_AMC_SET_SIZE * PS_AMC_NUM_SETS)

/** SP-AMC structures **/

// Entry of the table SP-AMC (to translate from structural address to physical address)
typedef struct {
	uint64_t tag;     /* Tag of the Structural address */
	uint64_t pa_line; /* Physical address (line, without cache offset bits) */
	bool     valid;   /* Whether this entry is valid or not */
} sp_amc_entry_t;

#define SP_AMC_ENTRY_SIZE (SP_AMC_TAG_BITS + ADDR_CACHE_LINE_BITS + 1)

// Set of SP-AMC entries (ways)
typedef struct {
	sp_amc_entry_t way[SP_AMC_NUM_WAYS];
	int            lru[SP_AMC_NUM_WAYS];
} sp_amc_set_t;

#define SP_AMC_SET_SIZE ((SP_AMC_ENTRY_SIZE + SP_AMC_NUM_WAYS_BITS) * SP_AMC_NUM_WAYS)

// Aggregation of SP-AMC sets. Set-associative cache.
typedef struct {
	sp_amc_set_t set[SP_AMC_NUM_SETS];
} sp_amc_t;

#define SP_AMC_SIZE (SP_AMC_SET_SIZE * SP_AMC_NUM_SETS)

/** Training Unit structures **/

// Training unit entry. LRU, less recent used
typedef struct {
	uint64_t pc;             /* Program counter */
	uint64_t last_addr_line; /* Last accessed address by this PC without cache line offset bits */
	bool     valid;          /* Whether this entry is valid or not */
} training_unit_entry_t;

#define TRAINING_UNIT_ENTRY_SIZE (ADDR_BITS + ADDR_CACHE_LINE_BITS + 1)

// Training unit
typedef struct {
	training_unit_entry_t entry[TRAINING_UNIT_NUM_STREAMS];
	int                     lru[TRAINING_UNIT_NUM_STREAMS];
} training_unit_t;

#define TRAINING_UNIT_SIZE ((TRAINING_UNIT_ENTRY_SIZE + TRAINING_UNIT_NUM_STREAMS_BITS) * TRAINING_UNIT_NUM_STREAMS)

/** Other structures **/

// Represents the full context
typedef struct {
	ps_amc_t        ps;
	sp_amc_t        sp;
	training_unit_t training_unit;
	uint64_t        sa_counter; // Counts allocated structural addresses
} amc_ctx_t;

#define AMC_CTX_SIZE (PS_AMC_SIZE + SP_AMC_SIZE + TRAINING_UNIT_SIZE + STRUCTURAL_ADDR_BITS)

/** Prefetcher functions **/

void amc_ctx_reset(amc_ctx_t *ctx);
void amc_ctx_train(amc_ctx_t *ctx, uint64_t pc, uint64_t addr_line);
int amc_ctx_predict(amc_ctx_t *ctx, uint64_t trigger_pa_line, uint64_t *prediction, int degree);

/** Debugging functions **/

void training_unit_print(const training_unit_t *tu);
void ps_amc_print(const ps_amc_t *ps);
void sp_amc_print(const sp_amc_t *sp);

#endif
