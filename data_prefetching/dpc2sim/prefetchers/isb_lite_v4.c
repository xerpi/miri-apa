/*
	Hybrid between ISB and AMPM
	Group 7 - Granell and Valle
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "../inc/prefetcher.h"

/* https://stackoverflow.com/a/39920811 */
#define NEEDS_BIT(N, B)      (((unsigned long)(N) >> (B)) > 0)
#define BITS_TO_REPRESENT(N)                   \
	(NEEDS_BIT(N,  0) + NEEDS_BIT(N,  1) + \
	 NEEDS_BIT(N,  2) + NEEDS_BIT(N,  3) + \
	 NEEDS_BIT(N,  4) + NEEDS_BIT(N,  5) + \
	 NEEDS_BIT(N,  6) + NEEDS_BIT(N,  7) + \
	 NEEDS_BIT(N,  8) + NEEDS_BIT(N,  9) + \
	 NEEDS_BIT(N, 10) + NEEDS_BIT(N, 11) + \
	 NEEDS_BIT(N, 12) + NEEDS_BIT(N, 13) + \
	 NEEDS_BIT(N, 14) + NEEDS_BIT(N, 15) + \
	 NEEDS_BIT(N, 16) + NEEDS_BIT(N, 17) + \
	 NEEDS_BIT(N, 18) + NEEDS_BIT(N, 19) + \
	 NEEDS_BIT(N, 20) + NEEDS_BIT(N, 21) + \
	 NEEDS_BIT(N, 22) + NEEDS_BIT(N, 23) + \
	 NEEDS_BIT(N, 24) + NEEDS_BIT(N, 25) + \
	 NEEDS_BIT(N, 26) + NEEDS_BIT(N, 27) + \
	 NEEDS_BIT(N, 28) + NEEDS_BIT(N, 29) + \
	 NEEDS_BIT(N, 30) + NEEDS_BIT(N, 31)   \
	)

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))
#define MASK(n)       ((1ULL << (n)) - 1)
#define MIN(x, y)     ((x) < (y) ? (x) : (y))

/*** ISB ***/

#define ADDR_BITS		64
#define STRUCTURAL_ADDR_BITS	64
#define CACHE_LINE_OFFSET_BITS	BITS_TO_REPRESENT(CACHE_LINE_SIZE - 1)
#define PAGE_OFFSET_BITS	BITS_TO_REPRESENT(PAGE_SIZE - 1)
#define ADDR_CACHE_LINE_BITS	(ADDR_BITS - CACHE_LINE_OFFSET_BITS)
#define PAGE_LINE_BITS		(PAGE_OFFSET_BITS - CACHE_LINE_OFFSET_BITS)
#define PAGE_LINE_MASK		MASK(PAGE_LINE_BITS)

/*
 * Physical address (line) mapping to PS-AMC (set-associative):
 * ____________________________________________              _________
 * |            TAG             |     Set     |, Discarded: | Offset |
 * --------------------------------------------              ---------
 *
 *
 * Structural address mapping to SP-AMC (set-associative):
 * _____________________________________________________
 * |                TAG                 |     Set      |
 * -----------------------------------------------------
 */
#ifdef WAYS
	#define PS_AMC_NUM_WAYS		WAYS
#else
	#define PS_AMC_NUM_WAYS		16
#endif
#define PS_AMC_NUM_WAYS_BITS	BITS_TO_REPRESENT(PS_AMC_NUM_WAYS - 1)
#ifdef SETS
	#define PS_AMC_NUM_SETS		SETS
#else
	#define PS_AMC_NUM_SETS		32
#endif
#define PS_AMC_SET_OFFSET	0
#define PS_AMC_SET_BITS		BITS_TO_REPRESENT(PS_AMC_NUM_SETS - 1)
#define PS_AMC_SET_MASK		MASK(PS_AMC_SET_BITS)
#define PS_AMC_TAG_OFFSET	(PS_AMC_SET_OFFSET + PS_AMC_SET_BITS)
#define PS_AMC_TAG_BITS		(ADDR_BITS - PS_AMC_TAG_OFFSET)
#define PS_AMC_TAG_MASK	 	MASK(PS_AMC_TAG_BITS)

#define PS_AMC_CONF_CNTR_BITS	2
#define PS_AMC_CONF_CNTR_MAX	((1U << PS_AMC_CONF_CNTR_BITS) - 1)

#ifdef WAYS
	#define SP_AMC_NUM_WAYS		WAYS
#else
	#define SP_AMC_NUM_WAYS		16
#endif
#define SP_AMC_NUM_WAYS_BITS	BITS_TO_REPRESENT(SP_AMC_NUM_WAYS - 1)
#ifdef SETS
	#define SP_AMC_NUM_SETS		SETS
#else
	#define SP_AMC_NUM_SETS		32
#endif
#define SP_AMC_SET_OFFSET 	0
#define SP_AMC_SET_BITS	 	BITS_TO_REPRESENT(SP_AMC_NUM_SETS - 1)
#define SP_AMC_SET_MASK	 	MASK(SP_AMC_SET_BITS)
#define SP_AMC_TAG_OFFSET	(SP_AMC_SET_OFFSET + SP_AMC_SET_BITS)
#define SP_AMC_TAG_BITS		(STRUCTURAL_ADDR_BITS - SP_AMC_SET_BITS)
#define SP_AMC_TAG_MASK	 	MASK(SP_AMC_TAG_BITS)

#ifdef TRAINING
	#define TRAINING_UNIT_NUM_STREAMS	TRAINING
#else
	#define TRAINING_UNIT_NUM_STREAMS	256
#endif
#define TRAINING_UNIT_NUM_STREAMS_BITS	BITS_TO_REPRESENT(TRAINING_UNIT_NUM_STREAMS - 1)

#define AMC_SA_CHUNK_SIZE	4096

/** PS-AMC structures **/

// Entry of the table PS-AMC (to translate from physical address to structural address)
typedef struct {
	uint64_t tag;     /* Tag of the Physical address */
	uint64_t sa;      /* Structural address */
	uint8_t  counter; /* Confidence counter */
	bool     valid;	  /* Whether this entry is valid or not */
} ps_amc_entry_t;

#define PS_AMC_ENTRY_SIZE (PS_AMC_TAG_BITS + STRUCTURAL_ADDR_BITS + PS_AMC_CONF_CNTR_BITS + 1)

// Set of PS-AMC entries (ways)
typedef struct {
	ps_amc_entry_t way[PS_AMC_NUM_WAYS];
	int            lru[PS_AMC_NUM_WAYS];
} ps_amc_set_t;

#define PS_AMC_SET_SIZE ((PS_AMC_ENTRY_SIZE + PS_AMC_NUM_WAYS_BITS) * PS_AMC_NUM_WAYS)

// Aggregation of PS-AMC sets. Set-associative cache.
typedef struct {
	ps_amc_set_t set[PS_AMC_NUM_SETS];
} ps_amc_t;

#define PS_AMC_SIZE (PS_AMC_SET_SIZE * PS_AMC_NUM_SETS)

/** SP-AMC structures **/

// Entry of the table SP-AMC (to translate from structural address to physical address)
typedef struct {
	uint64_t tag;     /* Tag of the Structural address */
	uint64_t pa_line; /* Physical address (line, without cache offset bits) */
	bool     valid;   /* Whether this entry is valid or not */
} sp_amc_entry_t;

#define SP_AMC_ENTRY_SIZE (SP_AMC_TAG_BITS + ADDR_CACHE_LINE_BITS + 1)

// Set of SP-AMC entries (ways)
typedef struct {
	sp_amc_entry_t way[SP_AMC_NUM_WAYS];
	int            lru[SP_AMC_NUM_WAYS];
} sp_amc_set_t;

#define SP_AMC_SET_SIZE ((SP_AMC_ENTRY_SIZE + SP_AMC_NUM_WAYS_BITS) * SP_AMC_NUM_WAYS)

// Aggregation of SP-AMC sets. Set-associative cache.
typedef struct {
	sp_amc_set_t set[SP_AMC_NUM_SETS];
} sp_amc_t;

#define SP_AMC_SIZE (SP_AMC_SET_SIZE * SP_AMC_NUM_SETS)

/** Training Unit structures **/

// Training unit entry. LRU, less recent used
typedef struct {
	uint64_t pc;             /* Program counter */
	uint64_t last_addr_line; /* Last accessed address by this PC without cache line offset bits */
	bool     valid;          /* Whether this entry is valid or not */
} training_unit_entry_t;

#define TRAINING_UNIT_ENTRY_SIZE (ADDR_BITS + ADDR_CACHE_LINE_BITS + 1)

// Training unit
typedef struct {
	training_unit_entry_t entry[TRAINING_UNIT_NUM_STREAMS];
	int                     lru[TRAINING_UNIT_NUM_STREAMS];
} training_unit_t;

#define TRAINING_UNIT_SIZE ((TRAINING_UNIT_ENTRY_SIZE + TRAINING_UNIT_NUM_STREAMS_BITS) * TRAINING_UNIT_NUM_STREAMS)

// Represents the full context
typedef struct {
	ps_amc_t        ps;
	sp_amc_t        sp;
	training_unit_t training_unit;
	uint64_t        sa_counter; // Counts allocated structural addresses
} amc_ctx_t;

#define AMC_CTX_SIZE (PS_AMC_SIZE + SP_AMC_SIZE + TRAINING_UNIT_SIZE + STRUCTURAL_ADDR_BITS)

void amc_ctx_reset(amc_ctx_t *ctx);
void amc_ctx_train(amc_ctx_t *ctx, uint64_t pc, uint64_t addr_line);
int amc_ctx_predict(amc_ctx_t *ctx, uint64_t trigger_pa_line, uint64_t *prediction, int degree);


/*** Pending prefetch queue ***/

/*
 * Physical address:
 * ___________________________________________________________________
 * |                       Page                    |   Page offset   |
 * -------------------------------------------------------------------
 * The prefetch queue is direct-mapped and stores {page -> page line}:
 * ___________________________________________________________________
 * |                  Tag                |  Entry  |  Line  | Offset |
 * -------------------------------------------------------------------
 *
 * We only need to save the cache line index within the page offset.
 */

#define PENDING_PREFETCH_QUEUE_ENTRY_NUM	128
#define PENDING_PREFETCH_ENTRY_OFFSET		0
#define PENDING_PREFETCH_ENTRY_BITS       BITS_TO_REPRESENT(PENDING_PREFETCH_QUEUE_ENTRY_NUM - 1)
#define PENDING_PREFETCH_ENTRY_MASK       MASK(PENDING_PREFETCH_ENTRY_BITS)
#define PENDING_PREFETCH_TAG_OFFSET       PENDING_PREFETCH_ENTRY_BITS
#define PENDING_PREFETCH_TAG_BITS         (ADDR_BITS - PAGE_OFFSET_BITS - PENDING_PREFETCH_ENTRY_BITS)
#define PENDING_PREFETCH_TAG_MASK         MASK(PENDING_PREFETCH_TAG_BITS)

typedef struct {
	uint64_t tag;  /* Tag of the page */
	uint64_t line; /* Line within the page */
	bool     valid;
} pending_prefetch_entry_t;

#define PENDING_PREFETCH_ENTRY_SIZE	(ADDR_BITS - CACHE_LINE_OFFSET_BITS + 1)

typedef struct {
	pending_prefetch_entry_t entry[PENDING_PREFETCH_QUEUE_ENTRY_NUM];
} pending_prefetch_queue_t;

#define PENDING_PREFETCH_QUEUE_SIZE	(PENDING_PREFETCH_ENTRY_SIZE * PENDING_PREFETCH_QUEUE_ENTRY_NUM)

void pending_prefetch_queue_reset(pending_prefetch_queue_t *queue);
void pending_prefetch_queue_map(pending_prefetch_queue_t *queue, uint64_t page, uint64_t line);
pending_prefetch_entry_t *pending_prefetch_queue_query(pending_prefetch_queue_t *queue, uint64_t page);
void pending_prefetch_queue_cache_fill(pending_prefetch_queue_t *queue, uint64_t addr_page);

/*** Hybrid ***/

#define ISB_NUM_PREDICTION	8
#define ISB_MSHR_THRESHOLD	8
#ifndef DENSITY_THRESHOLD
	#define DENSITY_THRESHOLD	15 // Out of 100
#endif

#define ISB_SIZE		(AMC_CTX_SIZE + PENDING_PREFETCH_QUEUE_SIZE)

static pending_prefetch_queue_t pending_prefetch_isb_queue;

static amc_ctx_t amc_ctx;

static inline int trigger_prefetch(uint64_t base_addr, uint64_t pf_addr, int mshr_threshold)
{
	/* Prefetch to L2 or LLC depending on the MSHR occupancy */
	if (get_l2_mshr_occupancy(0) < mshr_threshold)
		return l2_prefetch_line(0, base_addr, pf_addr, FILL_L2);
	else
		return l2_prefetch_line(0, base_addr, pf_addr, FILL_LLC);
}

void trigger_prefetch_queue(pending_prefetch_queue_t *queue, uint64_t base_addr, uint64_t addr_page)
{
	pending_prefetch_entry_t * pending = pending_prefetch_queue_query(queue, addr_page);
	if (pending) {
		/* Reconstruct the address to prefetch from */
		uint64_t pf_addr = (addr_page << PAGE_OFFSET_BITS) |
		                   (pending->line << CACHE_LINE_OFFSET_BITS);
		int ret = trigger_prefetch(base_addr, pf_addr, 8);

		/* If the prefetch was successful, remove it from the pending queue */
		if (ret)
			pending->valid = false;
	}
}

void isb_prefetching(amc_ctx_t *amc_ctx, uint64_t pc, uint64_t addr)
{
	int num_predicted;
	uint64_t prediction[ISB_NUM_PREDICTION];
	pending_prefetch_entry_t *pending;
	uint64_t addr_line = addr >> CACHE_LINE_OFFSET_BITS;
	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	// ISB Prediction
	num_predicted = amc_ctx_predict(amc_ctx, addr_line, prediction, ISB_NUM_PREDICTION);

	/* For each predicted address to prefetch */
	for (int i = 0; i < num_predicted; i++) {
		uint64_t pf_page = prediction[i] >> PAGE_LINE_BITS;

		/* If the predicted page is the same as the one we are operating, we can issue the prefetch */
		if (pf_page == addr_page) {
			/* Reconstruct the address to prefetch from */
			uint64_t pf_addr = prediction[i] << CACHE_LINE_OFFSET_BITS;

			/* Prefetch with MSHR threshold */
			trigger_prefetch(addr, pf_addr, ISB_MSHR_THRESHOLD);
		} else {
			/* Otherwise, We add the prefetch to the pending queue, while
			 * only allowing 1 pending prefetch per page at max. */
			pending = pending_prefetch_queue_query(&pending_prefetch_isb_queue, pf_page);
			if (!pending) {
				pending_prefetch_queue_map(&pending_prefetch_isb_queue, pf_page,
							   prediction[i] & PAGE_LINE_MASK);
			}
		}

	}

	// ISB Training
	amc_ctx_train(amc_ctx, pc, addr_line);
}

void l2_prefetcher_initialize(int cpu_num)
{
	printf("ISB Prefetcher, size: %d bits = %f KB\n", ISB_SIZE, (ISB_SIZE / 8) / 1024.0);
	// you can inspect these knob values from your code to see which configuration you're runnig in
	printf("Knobs visible from prefetcher: %d %d %d\n", knob_scramble_loads, knob_small_llc, knob_low_bandwidth);

	amc_ctx_reset(&amc_ctx);
	pending_prefetch_queue_reset(&pending_prefetch_isb_queue);
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr, unsigned long long int ip, int cache_hit)
{
	// uncomment this line to see all the information available to make prefetch decisions
	//printf("(0x%llx 0x%llx %d %d %d) ", addr, ip, cache_hit, get_l2_read_queue_occupancy(0), get_l2_mshr_occupancy(0));

	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	/* Pending prefetch queue */
	trigger_prefetch_queue(&pending_prefetch_isb_queue, addr, addr_page);

	/* ISB Prefetching */
	isb_prefetching(&amc_ctx, ip, addr);
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, int prefetch, unsigned long long int evicted_addr)
{
	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	pending_prefetch_queue_cache_fill(&pending_prefetch_isb_queue, addr_page);
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
	printf("Prefetcher heartbeat stats\n");
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
	printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
	printf("Prefetcher final stats\n");
}

/*** ISB implementation ***/

#ifdef DEBUG
#  define debugf printf
#else
#  define debugf(...)
#endif

#ifdef INFO
#  define infof printf
#else
#  define infof(...)
#endif

/* Initialize a LRU array */
static void lru_init(int *lru, int lru_size)
{
	for (int i = 0; i < lru_size; i++)
		lru[i] = lru_size - i - 1;
}

/* Returns the index of the Least Recently Used entry */
static int lru_get_victim(int *lru, int lru_size)
{
	/* The LRU entry is always at the end */
	return lru[lru_size - 1];
}

/* Updates the LRU when accessing an entry */
static void lru_access(int *lru, int lru_size, int access_index)
{
	int lru_index;

	/* First find the LRU-index of the accessed entry */
	for (lru_index = 0; lru_index < lru_size; lru_index++) {
		if (lru[lru_index] == access_index)
			break;
	}

	/* Move the accessed entry to the position 0 */
	while (lru_index > 0) {
		int tmp = lru[lru_index - 1];
		lru[lru_index - 1] = lru[lru_index];
		lru[lru_index] = tmp;
		lru_index--;
	}
}

// Resets the Training Unit table
static void training_unit_reset(training_unit_t *tu)
{
	for (int i = 0; i < TRAINING_UNIT_NUM_STREAMS; i++) {
		tu->entry[i].pc = 0;
		tu->entry[i].last_addr_line = 0;
		tu->entry[i].valid = false;
	}

	lru_init(tu->lru, TRAINING_UNIT_NUM_STREAMS);
}

static bool training_unit_find_pc(training_unit_t *tu, uint64_t pc, training_unit_entry_t **entry, int *entry_index)
{
	for (int i = 0; i < TRAINING_UNIT_NUM_STREAMS; i++) {
		if (pc == tu->entry[i].pc && tu->entry[i].valid) {
			*entry = &tu->entry[i];
			*entry_index = i;
			return true;
		}
	}

	return false;
}

static void training_unit_lru_get_victim(training_unit_t *tu, training_unit_entry_t **entry, int *entry_index)
{
	*entry_index = lru_get_victim(tu->lru, TRAINING_UNIT_NUM_STREAMS);
	*entry = &tu->entry[*entry_index];
}

static void training_unit_lru_access(training_unit_t *tu, int entry_index)
{
	lru_access(tu->lru, TRAINING_UNIT_NUM_STREAMS, entry_index);
}

// Resets the PS-AMC table
static void ps_amc_reset(ps_amc_t *ps)
{
	for (int i = 0; i < PS_AMC_NUM_SETS; i++) {
		for (int j = 0; j < PS_AMC_NUM_WAYS; j++) {
			ps->set[i].way[j].valid = 0;
			ps->set[i].way[j].counter = 0; // Not needed
			ps->set[i].way[j].tag = 0;     // Not needed
			ps->set[i].way[j].sa = 0;      // Not needed
		}

		lru_init(ps->set[i].lru, PS_AMC_NUM_WAYS);
	}
}

// Search a physical address (without cache offset bits) in the PS-AMC table
static ps_amc_entry_t *ps_amc_query(ps_amc_t *ps, uint64_t addr_line)
{
	uint64_t tag = (addr_line >> PS_AMC_TAG_OFFSET) & PS_AMC_TAG_MASK;
	uint64_t set = (addr_line >> PS_AMC_SET_OFFSET) & PS_AMC_SET_MASK;

	// Search tag in all the ways of the set
	for (int i = 0; i < PS_AMC_NUM_WAYS; i++) {
		if (tag == ps->set[set].way[i].tag && ps->set[set].way[i].valid) {
			return &(ps->set[set].way[i]);
		}
	}

	return NULL;
}

// Insert the mapping from Physical address (line) to a Structural address into the table
static void ps_amc_map(ps_amc_t *ps, uint64_t pa_line, uint64_t sa)
{
	uint64_t tag = (pa_line >> PS_AMC_TAG_OFFSET) & PS_AMC_TAG_MASK;
	uint64_t set = (pa_line >> PS_AMC_SET_OFFSET) & PS_AMC_SET_MASK;

	debugf("PS-AMC map: pa_line = 0x%010lx -> sa = 0x%010lx, tag: 0x%08lx, set: 0x%lx\n",
		pa_line, sa, tag, set);

	// Search tag in all the ways of the set, and if hit modify it
	for (int i = 0; i < PS_AMC_NUM_WAYS; i++) {
		if (ps->set[set].way[i].tag == tag && ps->set[set].way[i].valid) {
			ps->set[set].way[i].tag = tag;
			ps->set[set].way[i].sa = sa;
			// Set confidence counter to max value
			ps->set[set].way[i].counter = PS_AMC_CONF_CNTR_MAX;
			// Update LRU information
			lru_access(ps->set[set].lru, PS_AMC_NUM_WAYS, i);
			return;
		}
	}

	// Physical address not found, replacement
	int victim = lru_get_victim(ps->set[set].lru, PS_AMC_NUM_WAYS);
	ps->set[set].way[victim].tag = tag;
	ps->set[set].way[victim].sa = sa;
	// Set confidence counter to max value
	ps->set[set].way[victim].counter = PS_AMC_CONF_CNTR_MAX;
	ps->set[set].way[victim].valid = 1;

	// Update LRU information
	lru_access(ps->set[set].lru, PS_AMC_NUM_WAYS, victim);
}

// Resets the SP-AMC table
static void sp_amc_reset(sp_amc_t *sp)
{
	for (int i = 0; i < SP_AMC_NUM_SETS; i++) {
		for (int j = 0; j < SP_AMC_NUM_WAYS; j++) {
			sp->set[i].way[j].tag = 0;
			sp->set[i].way[j].pa_line = 0;
			sp->set[i].way[j].valid = false;
		}

		lru_init(sp->set[i].lru, SP_AMC_NUM_WAYS);
	}
}

// Search a structural address in the SP-AMC table
static sp_amc_entry_t *sp_amc_query(sp_amc_t *sp, uint64_t sa)
{
	uint64_t tag = (sa >> SP_AMC_TAG_OFFSET) & SP_AMC_TAG_MASK;
	uint64_t set = (sa >> SP_AMC_SET_OFFSET) & SP_AMC_SET_MASK;

	// Search tag in all the ways of the set
	for (int i = 0; i < SP_AMC_NUM_WAYS; i++) {
		if ((sp->set[set].way[i].tag == tag) && sp->set[set].way[i].valid) {
			lru_access(sp->set[set].lru, SP_AMC_NUM_WAYS, i);
			return &(sp->set[set].way[i]);
		}
	}

	return NULL;
}

// Insert the mapping from Structural address to a Physical address into the table
static void sp_amc_map(sp_amc_t *sp, uint64_t sa, uint64_t pa_line)
{
	uint64_t tag = (sa >> SP_AMC_TAG_OFFSET) & SP_AMC_TAG_MASK;
	uint64_t set = (sa >> SP_AMC_SET_OFFSET) & SP_AMC_SET_MASK;

	debugf("SP-AMC map: sa = 0x%010lx -> pa_line = 0x%010lx, tag: 0x%08lx, set: 0x%lx\n",
		sa, pa_line, tag, set);

	// Search tag in all the ways of the set, and if hit modify it
	for (int i = 0; i < SP_AMC_NUM_WAYS; i++) {
		if ((sp->set[set].way[i].tag == tag) && sp->set[set].way[i].valid) {
			sp->set[set].way[i].pa_line = pa_line;
			lru_access(sp->set[set].lru, SP_AMC_NUM_WAYS, i);
			return;
		}
	}

	// Structural address not found, replacement
	int victim = lru_get_victim(sp->set[set].lru, SP_AMC_NUM_WAYS);
	sp->set[set].way[victim].tag = tag;
	sp->set[set].way[victim].pa_line = pa_line;
	sp->set[set].way[victim].valid = true;

	// Update LRU information
	lru_access(sp->set[set].lru, SP_AMC_NUM_WAYS, victim);
}

void amc_ctx_reset(amc_ctx_t *ctx)
{
	ps_amc_reset(&ctx->ps);
	sp_amc_reset(&ctx->sp);
	training_unit_reset(&ctx->training_unit);
	ctx->sa_counter = 0;
}

static uint64_t amc_ctx_sa_alloc(amc_ctx_t *ctx)
{
	uint64_t sa = ctx->sa_counter;

	ctx->sa_counter += AMC_SA_CHUNK_SIZE;

	return sa;
}

static uint64_t amc_ctx_sa_next(uint64_t sa)
{
	uint64_t mod = sa % AMC_SA_CHUNK_SIZE;

	return (sa & ~((uint64_t)AMC_SA_CHUNK_SIZE - 1)) | (mod + 1);
}

void amc_ctx_train(amc_ctx_t *ctx, uint64_t pc, uint64_t addr_line)
{
	bool tu_entry_found;
	training_unit_entry_t *tu_entry;
	int tu_entry_index;
	uint64_t last_addr_line;
	ps_amc_entry_t *ps_last_entry;
	ps_amc_entry_t *ps_cur_entry;
	training_unit_t *tu = &ctx->training_unit;
	ps_amc_t *ps = &ctx->ps;
	sp_amc_t *sp = &ctx->sp;

	debugf("AMC Train: pc = 0x%010lx, addr_line = 0x%010lx\n", pc, addr_line);

	/* Find the PC in the Training Unit */
	tu_entry_found = training_unit_find_pc(tu, pc, &tu_entry, &tu_entry_index);

	/* PC not found in the Training Unit: find a victim */
	if (!tu_entry_found) {
		training_unit_lru_get_victim(tu, &tu_entry, &tu_entry_index);
		/* Update the entry's data */
		tu_entry->pc = pc;
		tu_entry->last_addr_line = addr_line;
		tu_entry->valid = true;
	} else {
		last_addr_line = tu_entry->last_addr_line;
		/* Update the TU's last address */
		tu_entry->last_addr_line = addr_line;
	}

	/* Update the LRU info */
	training_unit_lru_access(tu, tu_entry_index);

	/* If we did not find the PC in the Training Unit or
	 * if we are accessing the same address, we are done */
	if (!tu_entry_found || last_addr_line == addr_line)
		return;

	/* Query entries for last and current access */
	ps_last_entry = ps_amc_query(ps, last_addr_line); // A
	ps_cur_entry = ps_amc_query(ps, addr_line);       // B

	debugf("  last: %p, cur: %p\n", ps_last_entry, ps_cur_entry);

	/* Both in the PS-AMC */
	// Both addresses are correlated. The same pc has accessed both, and they are different.
	if (ps_last_entry && ps_cur_entry) {
		debugf("Case Both\n");
		uint64_t last_sa_next = amc_ctx_sa_next(ps_last_entry->sa);
		/* Check if they are consecutive */
		if (ps_cur_entry->sa == last_sa_next) {
			/* Increment current's counter */
			if (ps_cur_entry->counter < PS_AMC_CONF_CNTR_MAX)
				ps_cur_entry->counter++;
		} else { /* Non-consecutive */ // Then, make them consecutive!
			/* Decrement current's counter */
			if (--ps_cur_entry->counter == 0) {
				ps_cur_entry->sa = last_sa_next;
				ps_cur_entry->counter = PS_AMC_CONF_CNTR_MAX;
				sp_amc_map(sp, ps_cur_entry->sa, addr_line);
			}
		}
	} else if (!ps_last_entry && !ps_cur_entry) { /* Neither in PS-AMC */
		debugf("Case Neither\n");
		uint64_t next_sa = amc_ctx_sa_alloc(ctx);
		ps_amc_map(ps, last_addr_line, next_sa);
		ps_amc_map(ps, addr_line, next_sa + 1);
		sp_amc_map(sp, next_sa, last_addr_line);
		sp_amc_map(sp, next_sa + 1, addr_line);
	} else if (ps_last_entry) { /* Only ps_last_entry */
		debugf("Case Only last\n");
		uint64_t next_sa = amc_ctx_sa_next(ps_last_entry->sa);
		ps_amc_map(ps, addr_line, next_sa);
		sp_amc_map(sp, next_sa, addr_line);
	} else { /* Only ps_cur_entry */
		debugf("Case Only current\n");
		uint64_t next_sa = amc_ctx_sa_alloc(ctx);
		ps_amc_map(ps, last_addr_line, next_sa);
		sp_amc_map(sp, next_sa, last_addr_line);
		/* Decrement current's counter */
		if (--ps_cur_entry->counter == 0) {
			ps_last_entry = ps_amc_query(ps, last_addr_line);
			ps_cur_entry->sa = amc_ctx_sa_next(ps_last_entry->sa);
			ps_cur_entry->counter = PS_AMC_CONF_CNTR_MAX;
			sp_amc_map(sp, ps_cur_entry->sa, addr_line);
		}
	}
}

int amc_ctx_predict(amc_ctx_t *ctx, uint64_t trigger_pa_line, uint64_t *prediction, int degree)
{
	ps_amc_t *ps = &ctx->ps;
	sp_amc_t *sp = &ctx->sp;
	ps_amc_entry_t *trigger_entry;
	int i, count;

	// Retrieve the trigger address' structural address
	trigger_entry = ps_amc_query(ps, trigger_pa_line);

	// If not found in the PS-AMC, we are done
	if (!trigger_entry || !trigger_entry->valid)
		return 0;

	// Predict the next consecutive structural addresses to prefetch
	count = 0;
	for (i = 0; i < degree; i++) {
		// For degree k prefetching, the prediction includes the next k structural addresses
		uint64_t sa = trigger_entry->sa + i + 1;
		// Query the SP-AMC for the next Structural Address
		sp_amc_entry_t *sp_entry = sp_amc_query(sp, sa);
		// If we don't find the Structural Address in the SP-AMC, continue
		if (!sp_entry || !sp_entry->valid)
			continue; // Continue or break?
		// Add the Physical Address to the prediction list
		prediction[count++] = sp_entry->pa_line;
	}

	return count;
}

/*** Pending Prefetch Queue implementation */

void pending_prefetch_queue_reset(pending_prefetch_queue_t *queue)
{
	for (int i = 0; i < PENDING_PREFETCH_QUEUE_ENTRY_NUM; i++) {
		queue->entry[i].tag = 0;
		queue->entry[i].line = 0;
		queue->entry[i].valid = false;
	}
}

void pending_prefetch_queue_map(pending_prefetch_queue_t *queue, uint64_t page, uint64_t line)
{
	uint64_t entry = (page >> PENDING_PREFETCH_ENTRY_OFFSET) & PENDING_PREFETCH_ENTRY_MASK;
	uint64_t tag = (page >> PENDING_PREFETCH_TAG_OFFSET) & PENDING_PREFETCH_TAG_MASK;

	//printf("queue map, page: %lx, tag: %lx, line: %lx\n", page, tag, line);

	queue->entry[entry].tag = tag;
	queue->entry[entry].line = line;
	queue->entry[entry].valid = true;
}

pending_prefetch_entry_t *pending_prefetch_queue_query(pending_prefetch_queue_t *queue, uint64_t page)
{
	uint64_t entry = (page >> PENDING_PREFETCH_ENTRY_OFFSET) & PENDING_PREFETCH_ENTRY_MASK;
	uint64_t tag = (page >> PENDING_PREFETCH_TAG_OFFSET) & PENDING_PREFETCH_TAG_MASK;

	if ((tag == queue->entry[entry].tag) && queue->entry[entry].valid)
		return &queue->entry[entry];

	return NULL;
}

void pending_prefetch_queue_cache_fill(pending_prefetch_queue_t *queue, uint64_t addr_page)
{
	pending_prefetch_entry_t *pending;

	/* Check if we have pending prefetches at the same page we are operating */
	pending = pending_prefetch_queue_query(queue, addr_page);
	if (pending) {
		/* Remove it from the pending queue */
		pending->valid = false;
	}
}
