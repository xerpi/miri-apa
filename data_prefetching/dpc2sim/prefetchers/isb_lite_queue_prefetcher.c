#include <stdio.h>
#include "../inc/prefetcher.h"
#include "isb_lite.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

/*
 * Physical address:
 * ___________________________________________________________________
 * |                       Page                    |   Page offset   |
 * -------------------------------------------------------------------
 * The prefetch queue is direct-mapped and stores {page -> page line}:
 * ___________________________________________________________________
 * |                  Tag                |  Entry  |  Line  | Offset |
 * -------------------------------------------------------------------
 *
 * We only need to save the cache line index within the page offset.
 */

#define PENDING_PREFETCH_QUEUE_ENTRY_NUM	128
#define PENDING_PREFETCH_ENTRY_OFFSET		0
#define PENDING_PREFETCH_ENTRY_BITS		BITS_TO_REPRESENT(PENDING_PREFETCH_QUEUE_ENTRY_NUM - 1)
#define PENDING_PREFETCH_ENTRY_MASK		MASK(PENDING_PREFETCH_ENTRY_BITS)
#define PENDING_PREFETCH_TAG_OFFSET		PENDING_PREFETCH_ENTRY_BITS
#define PENDING_PREFETCH_TAG_BITS		(ADDR_BITS - PAGE_OFFSET_BITS - PENDING_PREFETCH_ENTRY_BITS)
#define PENDING_PREFETCH_TAG_MASK		MASK(PENDING_PREFETCH_TAG_BITS)

typedef struct {
	uint64_t tag;  /* Tag of the page */
	uint64_t line; /* Line within the page */
	bool     valid;
} pending_prefetch_entry_t;

#define PENDING_PREFETCH_ENTRY_SIZE	(ADDR_BITS - CACHE_LINE_OFFSET_BITS + 1)

typedef struct {
	pending_prefetch_entry_t entry[PENDING_PREFETCH_QUEUE_ENTRY_NUM];
} pending_prefetch_queue_t;

#define PENDING_PREFETCH_QUEUE_SIZE	(PENDING_PREFETCH_ENTRY_SIZE * PENDING_PREFETCH_QUEUE_ENTRY_NUM)

#define ISB_LITE_QUEUE_SIZE		(AMC_CTX_SIZE + PENDING_PREFETCH_QUEUE_SIZE)

static amc_ctx_t ctx;
static pending_prefetch_queue_t pending_prefetch_queue;

static void pending_prefetch_queue_reset(pending_prefetch_queue_t *queue)
{
	for (int i = 0; i < PENDING_PREFETCH_QUEUE_ENTRY_NUM; i++) {
		queue->entry[i].tag = 0;
		queue->entry[i].line = 0;
		queue->entry[i].valid = false;
	}
}

static pending_prefetch_entry_t *pending_prefetch_queue_query(pending_prefetch_queue_t *queue, uint64_t page)
{
	uint64_t entry = (page >> PENDING_PREFETCH_ENTRY_OFFSET) & PENDING_PREFETCH_ENTRY_MASK;
	uint64_t tag = (page >> PENDING_PREFETCH_TAG_OFFSET) & PENDING_PREFETCH_TAG_MASK;

	if ((tag == queue->entry[entry].tag) && queue->entry[entry].valid)
		return &queue->entry[entry];

	return NULL;
}

static void pending_prefetch_queue_map(pending_prefetch_queue_t *queue, uint64_t page, uint64_t line)
{
	uint64_t entry = (page >> PENDING_PREFETCH_ENTRY_OFFSET) & PENDING_PREFETCH_ENTRY_MASK;
	uint64_t tag = (page >> PENDING_PREFETCH_TAG_OFFSET) & PENDING_PREFETCH_TAG_MASK;

	//printf("queue map, page: %lx, tag: %lx, line: %lx\n", page, tag, line);

	queue->entry[entry].tag = tag;
	queue->entry[entry].line = line;
	queue->entry[entry].valid = true;
}

static void pending_prefetch_print(const pending_prefetch_queue_t *queue)
{
	for (int i = 0; i < PENDING_PREFETCH_QUEUE_ENTRY_NUM; i++) {
		const pending_prefetch_entry_t *entry = &queue->entry[i];
		printf("{tag = 0x%010lx, line = 0x%02lx, valid = %d}\n",
			entry->tag, entry->line, entry->valid);
	}

	printf("\n");
}

void l2_prefetcher_initialize(int cpu_num)
{
	printf("ISB Lite, size: %d bits = %f KB\n", ISB_LITE_QUEUE_SIZE, (ISB_LITE_QUEUE_SIZE / 8) / 1024.0);
	// you can inspect these knob values from your code to see which configuration you're runnig in
	printf("Knobs visible from prefetcher: %d %d %d\n", knob_scramble_loads, knob_small_llc, knob_low_bandwidth);

	amc_ctx_reset(&ctx);
	pending_prefetch_queue_reset(&pending_prefetch_queue);
}

static inline int trigger_prefetch(uint64_t base_addr, uint64_t pf_addr, int mshr_threshold)
{
	/* Prefetch to L2 or LLC depending on the MSHR occupancy */
	if (get_l2_mshr_occupancy(0) < mshr_threshold)
		return l2_prefetch_line(0, base_addr, pf_addr, FILL_L2);
	else
		return l2_prefetch_line(0, base_addr, pf_addr, FILL_LLC);
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr, unsigned long long int ip, int cache_hit)
{
	// uncomment this line to see all the information available to make prefetch decisions
	//printf("(0x%llx 0x%llx %d %d %d) ", addr, ip, cache_hit, get_l2_read_queue_occupancy(0), get_l2_mshr_occupancy(0));

	int ret;
	int num_predicted;
	uint64_t prediction[4];
	pending_prefetch_entry_t *pending;
	uint64_t addr_line = addr >> CACHE_LINE_OFFSET_BITS;
	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	//printf("Access pc = 0x%010llx, addr = 0x%010llx, addr_line = 0x%010llx\n", ip, addr, addr_line);

	/** Pending prefetch queue **/

	/* Check if we have a pending prefetch at the same page we are operating */
	pending = pending_prefetch_queue_query(&pending_prefetch_queue, addr_page);
	if (pending) {
		/* Reconstruct the address to prefetch from */
		uint64_t pf_addr = (addr_page << PAGE_OFFSET_BITS) |
				   (pending->line << CACHE_LINE_OFFSET_BITS);

		//printf("Got pending prefetch, op_addr: %llx, pf_addr: %lx\n", addr, pf_addr);

		/* Prefetch with MSHR threshold = 12 */
		ret = trigger_prefetch(addr, pf_addr, 12);

		/* If the prefetch was successful, remove it from the pending queue */
		if (ret)
			pending->valid = false;

		//printf("    l2_prefetch_line: %d\n", ret);
	}

	/** Predict **/
	num_predicted = amc_ctx_predict(&ctx, addr_line, prediction, ARRAY_SIZE(prediction));

	//if (num_predicted > 0)
	//	printf("Num predicted: %d\n", num_predicted);

	/* For each predicted address to prefetch */
	for (int i = 0; i < num_predicted; i++) {
		uint64_t pf_page = prediction[i] >> PAGE_LINE_BITS;

		//printf("Predicted: op page: %lx vs pf_page: %lx (pf_line %lx)\n", addr_page, pf_page, prediction[i]);

		/* If the predicted page is the same as the one we are operating, we can issue the prefetch */
		if (pf_page == addr_page) {
			/* Reconstruct the address to prefetch from */
			uint64_t pf_addr = prediction[i] << CACHE_LINE_OFFSET_BITS;

			/* Prefetch with MSHR threshold = 14 */
			ret = trigger_prefetch(addr, pf_addr, 14);

			//printf("    l2_prefetch_line: %d\n", ret);
		} else {
			/* Otherwise, We add the prefetch to the pending queue, while
			 * only allowing 1 pending prefetch per page at max. */
			pending = pending_prefetch_queue_query(&pending_prefetch_queue, pf_page);
			//uint64_t tag = (pf_page >> PENDING_PREFETCH_TAG_OFFSET) & PENDING_PREFETCH_TAG_MASK;
			//if (!pending || (pending->valid && pending->tag != tag)) {
			if (!pending) {
				printf("  add to prefetch queue\n");
				pending_prefetch_queue_map(&pending_prefetch_queue, pf_page,
							   prediction[i] & PAGE_LINE_MASK);
			}
		}

		//printf("  [%d]: addr: 0x%010llx (page 0x%09llx), prefetch addr: 0x%010llx (page 0x%09llx)\n",
		//	i, addr, addr >> PAGE_OFFSET_BITS, pf_addr, pf_addr >> PAGE_OFFSET_BITS);
	}

	/** Train **/
#ifdef INFO
	printf("***** TRAIN with pc = 0x%010llx, addr = 0x%010llx, addr_line = 0x%010llx, (page 0x%09llx) ******\n", ip, addr, addr_line, addr >> 12);
#endif
	amc_ctx_train(&ctx, ip, addr_line);

#ifdef INFO
	/* Print data structures */
	training_unit_print(&ctx.training_unit);
	ps_amc_print(&ctx.ps);
	sp_amc_print(&ctx.sp);
#endif
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, int prefetch, unsigned long long int evicted_addr)
{
	// uncomment this line to see the information available to you when there is a cache fill event
	//printf("0x%llx %d %d %d 0x%llx\n", addr, set, way, prefetch, evicted_addr);

	pending_prefetch_entry_t *pending;
	uint64_t addr_page = addr >> PAGE_OFFSET_BITS;

	/* Check if we have pending prefetches at the same page we are operating */
	pending = pending_prefetch_queue_query(&pending_prefetch_queue, addr_page);
	if (pending) {
		//printf("Removing pending tag %lx\n", pending->tag);
		/* Remove it from the pending queue */
		pending->valid = false;
	}
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
	printf("Prefetcher heartbeat stats\n");
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
	printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
	printf("Prefetcher final stats\n");
}
