#ifndef AMPM_LITE_H
#define AMPM_LITE_H

#include <stdint.h>
#include <stdbool.h>
#include "prefetcher.h"
#include "pending_prefetch_queue.h"

#define AMPM_PAGE_COUNT 64
#define AMPM_ACCESS_MAP (PAGE_SIZE / CACHE_LINE_SIZE)
#define AMPM_PREFETCH_DEGREE 2

#define AMPM_MAX_ADDRESS_RANGE 16
#define AMPM_MAX_PREFETCHES_NUM 8

typedef struct ampm_page
{
  // page address
  unsigned long long int page;

  // The access map itself.
  // Each element is set when the corresponding cache line is accessed.
  // The whole structure is analyzed to make prefetching decisions.
  // While this is coded as an integer array, it is used conceptually as a single 64-bit vector.
  int access_map[AMPM_ACCESS_MAP];

  // This map represents cache lines in this page that have already been prefetched.
  // We will only prefetch lines that haven't already been either demand accessed or prefetched.
  int pf_map[AMPM_ACCESS_MAP];

  // used for page replacement
  unsigned long long int lru;
} ampm_page_t;

#define AMPM_ENTRY_SIZE (ADDR_BITS + AMPM_ACCESS_MAP + AMPM_ACCESS_MAP)
#define AMPM_SIZE       (AMPM_PAGE_COUNT * AMPM_ENTRY_SIZE)

void ampm_initialize(ampm_page_t * ampm_pages);
int ampm_is_page_hit(ampm_page_t * ampm_pages, uint64_t page);
int ampm_find_oldest_page(ampm_page_t * ampm_pages);
void ampm_reset_page(ampm_page_t * ampm_pages, int page_index, uint64_t page);
int ampm_get_page_index(ampm_page_t *ampm_pages, uint64_t addr);
int ampm_prefetching(ampm_page_t *ampm_pages, int page_index, uint64_t addr,
        uint64_t prediction[2 * AMPM_MAX_ADDRESS_RANGE], int *num_predicted);
uint64_t ampm_get_density(ampm_page_t * ampm_pages, int page_index);

#endif
