#include <stdio.h>
#include "../inc/prefetcher.h"
#include "isb_lite.h"
#include <stdlib.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(*x))

#define MAX_QUEUE 10

static amc_ctx_t ctx;

void l2_prefetcher_initialize(int cpu_num)
{
	printf("ISB Lite, size: %d bits = %f KB\n", AMC_CTX_SIZE, (AMC_CTX_SIZE / 8) / 1024.0);
	// you can inspect these knob values from your code to see which configuration you're runnig in
	printf("Knobs visible from prefetcher: %d %d %d\n", knob_scramble_loads, knob_small_llc, knob_low_bandwidth);

	amc_ctx_reset(&ctx);
}

bool is_same_page (uint64_t addr, uint64_t pf_addr) {
	uint64_t shift = BITS_TO_REPRESENT(PAGE_SIZE);

	return (addr >> shift) == (pf_addr >> shift);
}

void process_prefetch (int cpu_num, uint64_t addr, uint64_t pf_addr) {
	int ret = 0;

#ifdef INFO
	printf("  [%d]: addr: 0x%010llx (page 0x%09llx), prefetch addr: 0x%010llx (page 0x%09llx)\n",
		i, addr, addr >> PAGE_OFFSET_BITS, pf_addr, pf_addr >> PAGE_OFFSET_BITS);
#endif

	if ((get_l2_mshr_occupancy(cpu_num) < 15) && (get_l2_read_queue_occupancy(cpu_num) < 31)) {
		ret = l2_prefetch_line(0, addr, pf_addr, FILL_L2);
	}
	else {
		ret = l2_prefetch_line(0, addr, pf_addr, FILL_LLC);
	}

	printf("    l2_prefetch_line: %d\n", ret);
}

void l2_prefetcher_operate(int cpu_num, unsigned long long int addr, unsigned long long int ip, int cache_hit)
{
	// uncomment this line to see all the information available to make prefetch decisions
	//printf("(0x%llx 0x%llx %d %d %d) ", addr, ip, cache_hit, get_l2_read_queue_occupancy(0), get_l2_mshr_occupancy(0));

	uint64_t addr_line = addr >> CACHE_LINE_OFFSET_BITS;
	uint64_t * waiting_queue = (uint64_t*)(malloc(sizeof(uint64_t) * MAX_QUEUE));
	int waiting_queue_occupancy = 0;

	/** Predict **/

	uint64_t prediction[4];
	int num_predicted;
	num_predicted = amc_ctx_predict(&ctx, addr_line, prediction, ARRAY_SIZE(prediction));

#ifdef INFO
	//printf("Access pc = 0x%010llx, addr = 0x%010llx, addr_line = 0x%010llx\n", ip, addr, addr_line);

	if (num_predicted > 0)
		printf("Num predicted: %d\n", num_predicted);
#endif

#ifdef INFO
	printf("Checking %d prefetch actions waiting at queue\n", waiting_queue_occupancy);
#endif
	for (int i = 0; i < waiting_queue_occupancy; i++) {
		uint64_t pf_addr = waiting_queue[i];

		if (is_same_page(addr, pf_addr)){
#ifdef INFO
			printf("Prefetch address is from the current page\n");
#endif
			process_prefetch(cpu_num, addr, pf_addr);
		}
	}

#ifdef INFO
	printf("Checking %d prefetch actions obtained in this cycle\n", num_predicted);
#endif
	for (int i = 0; i < num_predicted; i++) {
		uint64_t pf_addr = prediction[i] << CACHE_LINE_OFFSET_BITS;

		if (is_same_page(addr, pf_addr)){
			process_prefetch(cpu_num, addr, pf_addr);
		}
		else if (waiting_queue_occupancy < MAX_QUEUE - 1){
			waiting_queue[waiting_queue_occupancy++] = pf_addr;
		}
	}

	/** Train **/
#ifdef INFO
	printf("***** TRAIN with pc = 0x%010llx, addr = 0x%010llx, addr_line = 0x%010llx, (page 0x%09llx) ******\n", ip, addr, addr_line, addr >> 12);
#endif
	amc_ctx_train(&ctx, ip, addr_line);

#ifdef INFO
	/* Print data structures */
	training_unit_print(&ctx.training_unit);
	ps_amc_print(&ctx.ps);
	sp_amc_print(&ctx.sp);
#endif

	free(waiting_queue);
}

void l2_cache_fill(int cpu_num, unsigned long long int addr, int set, int way, int prefetch, unsigned long long int evicted_addr)
{
	// uncomment this line to see the information available to you when there is a cache fill event
	//printf("0x%llx %d %d %d 0x%llx\n", addr, set, way, prefetch, evicted_addr);
}

void l2_prefetcher_heartbeat_stats(int cpu_num)
{
	printf("Prefetcher heartbeat stats\n");
}

void l2_prefetcher_warmup_stats(int cpu_num)
{
	printf("Prefetcher warmup complete stats\n\n");
}

void l2_prefetcher_final_stats(int cpu_num)
{
	printf("Prefetcher final stats\n");
}
