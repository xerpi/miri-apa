#include <stdlib.h>
#include <stdio.h>
#include "pending_prefetch_queue.h"

void pending_prefetch_queue_reset(pending_prefetch_queue_t *queue)
{
	for (int i = 0; i < PENDING_PREFETCH_QUEUE_ENTRY_NUM; i++) {
		queue->entry[i].tag = 0;
		queue->entry[i].line = 0;
		queue->entry[i].valid = false;
	}
}

void pending_prefetch_queue_map(pending_prefetch_queue_t *queue, uint64_t page, uint64_t line)
{
	uint64_t entry = (page >> PENDING_PREFETCH_ENTRY_OFFSET) & PENDING_PREFETCH_ENTRY_MASK;
	uint64_t tag = (page >> PENDING_PREFETCH_TAG_OFFSET) & PENDING_PREFETCH_TAG_MASK;

	//printf("queue map, page: %lx, tag: %lx, line: %lx\n", page, tag, line);

	queue->entry[entry].tag = tag;
	queue->entry[entry].line = line;
	queue->entry[entry].valid = true;
}

pending_prefetch_entry_t *pending_prefetch_queue_query(pending_prefetch_queue_t *queue, uint64_t page)
{
	uint64_t entry = (page >> PENDING_PREFETCH_ENTRY_OFFSET) & PENDING_PREFETCH_ENTRY_MASK;
	uint64_t tag = (page >> PENDING_PREFETCH_TAG_OFFSET) & PENDING_PREFETCH_TAG_MASK;

	if ((tag == queue->entry[entry].tag) && queue->entry[entry].valid)
		return &queue->entry[entry];

	return NULL;
}

void pending_prefetch_queue_cache_fill(pending_prefetch_queue_t *queue, uint64_t addr_page)
{
	pending_prefetch_entry_t *pending;

	/* Check if we have pending prefetches at the same page we are operating */
	pending = pending_prefetch_queue_query(queue, addr_page);
	if (pending) {
		/* Remove it from the pending queue */
		pending->valid = false;
	}
}

void pending_prefetch_queue_print(const pending_prefetch_queue_t *queue)
{
	for (int i = 0; i < PENDING_PREFETCH_QUEUE_ENTRY_NUM; i++) {
		const pending_prefetch_entry_t *entry = &queue->entry[i];
		printf("{tag = 0x%010lx, line = 0x%02lx, valid = %d}\n",
			entry->tag, entry->line, entry->valid);
	}

	printf("\n");
}
