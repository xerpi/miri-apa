#include <stddef.h>
#include <stdio.h>
#include "isb_lite.h"

#ifdef DEBUG
#  define debugf printf
#else
#  define debugf(...)
#endif

#ifdef INFO
#  define infof printf
#else
#  define infof(...)
#endif

#define MIN(x, y)     ((x) < (y) ? (x) : (y))

/* Initialize a LRU array */
static void lru_init(int *lru, int lru_size)
{
	for (int i = 0; i < lru_size; i++)
		lru[i] = lru_size - i - 1;
}

/* Returns the index of the Least Recently Used entry */
static int lru_get_victim(int *lru, int lru_size)
{
	/* The LRU entry is always at the end */
	return lru[lru_size - 1];
}

/* Updates the LRU when accessing an entry */
static void lru_access(int *lru, int lru_size, int access_index)
{
	int lru_index;

	/* First find the LRU-index of the accessed entry */
	for (lru_index = 0; lru_index < lru_size; lru_index++) {
		if (lru[lru_index] == access_index)
			break;
	}

	/* Move the accessed entry to the position 0 */
	while (lru_index > 0) {
		int tmp = lru[lru_index - 1];
		lru[lru_index - 1] = lru[lru_index];
		lru[lru_index] = tmp;
		lru_index--;
	}
}

// Resets the Training Unit table
static void training_unit_reset(training_unit_t *tu)
{
	for (int i = 0; i < TRAINING_UNIT_NUM_STREAMS; i++) {
		tu->entry[i].pc = 0;
		tu->entry[i].last_addr_line = 0;
		tu->entry[i].valid = false;
	}

	lru_init(tu->lru, TRAINING_UNIT_NUM_STREAMS);
}

static bool training_unit_find_pc(training_unit_t *tu, uint64_t pc, training_unit_entry_t **entry, int *entry_index)
{
	for (int i = 0; i < TRAINING_UNIT_NUM_STREAMS; i++) {
		if (pc == tu->entry[i].pc && tu->entry[i].valid) {
			*entry = &tu->entry[i];
			*entry_index = i;
			return true;
		}
	}

	return false;
}

static void training_unit_lru_get_victim(training_unit_t *tu, training_unit_entry_t **entry, int *entry_index)
{
	*entry_index = lru_get_victim(tu->lru, TRAINING_UNIT_NUM_STREAMS);
	*entry = &tu->entry[*entry_index];
}

static void training_unit_lru_access(training_unit_t *tu, int entry_index)
{
	lru_access(tu->lru, TRAINING_UNIT_NUM_STREAMS, entry_index);
}

// Resets the PS-AMC table
static void ps_amc_reset(ps_amc_t *ps)
{
	for (int i = 0; i < PS_AMC_NUM_SETS; i++) {
		for (int j = 0; j < PS_AMC_NUM_WAYS; j++) {
			ps->set[i].way[j].valid = 0;
			ps->set[i].way[j].counter = 0; // Not needed
			ps->set[i].way[j].tag = 0;     // Not needed
			ps->set[i].way[j].sa = 0;      // Not needed
		}

		lru_init(ps->set[i].lru, PS_AMC_NUM_WAYS);
	}
}

// Search a physical address (without cache offset bits) in the PS-AMC table
static ps_amc_entry_t *ps_amc_query(ps_amc_t *ps, uint64_t addr_line)
{
	uint64_t tag = (addr_line >> PS_AMC_TAG_OFFSET) & PS_AMC_TAG_MASK;
	uint64_t set = (addr_line >> PS_AMC_SET_OFFSET) & PS_AMC_SET_MASK;

	// Search tag in all the ways of the set
	for (int i = 0; i < PS_AMC_NUM_WAYS; i++) {
		if (tag == ps->set[set].way[i].tag && ps->set[set].way[i].valid) {
			return &(ps->set[set].way[i]);
		}
	}

	return NULL;
}

// Insert the mapping from Physical address (line) to a Structural address into the table
static void ps_amc_map(ps_amc_t *ps, uint64_t pa_line, uint64_t sa)
{
	uint64_t tag = (pa_line >> PS_AMC_TAG_OFFSET) & PS_AMC_TAG_MASK;
	uint64_t set = (pa_line >> PS_AMC_SET_OFFSET) & PS_AMC_SET_MASK;

	debugf("PS-AMC map: pa_line = 0x%010lx -> sa = 0x%010lx, tag: 0x%08lx, set: 0x%lx\n",
		pa_line, sa, tag, set);

	// Search tag in all the ways of the set, and if hit modify it
	for (int i = 0; i < PS_AMC_NUM_WAYS; i++) {
		if (ps->set[set].way[i].tag == tag && ps->set[set].way[i].valid) {
			ps->set[set].way[i].tag = tag;
			ps->set[set].way[i].sa = sa;
			// Set confidence counter to max value
			ps->set[set].way[i].counter = PS_AMC_CONF_CNTR_MAX;
			// Update LRU information
			lru_access(ps->set[set].lru, PS_AMC_NUM_WAYS, i);
			return;
		}
	}

	// Physical address not found, replacement
	int victim = lru_get_victim(ps->set[set].lru, PS_AMC_NUM_WAYS);
	ps->set[set].way[victim].tag = tag;
	ps->set[set].way[victim].sa = sa;
	// Set confidence counter to max value
	ps->set[set].way[victim].counter = PS_AMC_CONF_CNTR_MAX;
	ps->set[set].way[victim].valid = 1;

	// Update LRU information
	lru_access(ps->set[set].lru, PS_AMC_NUM_WAYS, victim);
}

// Resets the SP-AMC table
static void sp_amc_reset(sp_amc_t *sp)
{
	for (int i = 0; i < SP_AMC_NUM_SETS; i++) {
		for (int j = 0; j < SP_AMC_NUM_WAYS; j++) {
			sp->set[i].way[j].tag = 0;
			sp->set[i].way[j].pa_line = 0;
			sp->set[i].way[j].valid = false;
		}

		lru_init(sp->set[i].lru, SP_AMC_NUM_WAYS);
	}
}

// Search a structural address in the SP-AMC table
static sp_amc_entry_t *sp_amc_query(sp_amc_t *sp, uint64_t sa)
{
	uint64_t tag = (sa >> SP_AMC_TAG_OFFSET) & SP_AMC_TAG_MASK;
	uint64_t set = (sa >> SP_AMC_SET_OFFSET) & SP_AMC_SET_MASK;

	// Search tag in all the ways of the set
	for (int i = 0; i < SP_AMC_NUM_WAYS; i++) {
		if ((sp->set[set].way[i].tag == tag) && sp->set[set].way[i].valid) {
			lru_access(sp->set[set].lru, SP_AMC_NUM_WAYS, i);
			return &(sp->set[set].way[i]);
		}
	}

	return NULL;
}

// Insert the mapping from Structural address to a Physical address into the table
static void sp_amc_map(sp_amc_t *sp, uint64_t sa, uint64_t pa_line)
{
	uint64_t tag = (sa >> SP_AMC_TAG_OFFSET) & SP_AMC_TAG_MASK;
	uint64_t set = (sa >> SP_AMC_SET_OFFSET) & SP_AMC_SET_MASK;

	debugf("SP-AMC map: sa = 0x%010lx -> pa_line = 0x%010lx, tag: 0x%08lx, set: 0x%lx\n",
		sa, pa_line, tag, set);

	// Search tag in all the ways of the set, and if hit modify it
	for (int i = 0; i < SP_AMC_NUM_WAYS; i++) {
		if ((sp->set[set].way[i].tag == tag) && sp->set[set].way[i].valid) {
			sp->set[set].way[i].pa_line = pa_line;
			lru_access(sp->set[set].lru, SP_AMC_NUM_WAYS, i);
			return;
		}
	}

	// Structural address not found, replacement
	int victim = lru_get_victim(sp->set[set].lru, SP_AMC_NUM_WAYS);
	sp->set[set].way[victim].tag = tag;
	sp->set[set].way[victim].pa_line = pa_line;
	sp->set[set].way[victim].valid = true;

	// Update LRU information
	lru_access(sp->set[set].lru, SP_AMC_NUM_WAYS, victim);
}

void amc_ctx_reset(amc_ctx_t *ctx)
{
	ps_amc_reset(&ctx->ps);
	sp_amc_reset(&ctx->sp);
	training_unit_reset(&ctx->training_unit);
	ctx->sa_counter = 0;
}

static uint64_t amc_ctx_sa_alloc(amc_ctx_t *ctx)
{
	uint64_t sa = ctx->sa_counter;

	ctx->sa_counter += AMC_SA_CHUNK_SIZE;

	return sa;
}

static uint64_t amc_ctx_sa_next(uint64_t sa)
{
	uint64_t mod = sa % AMC_SA_CHUNK_SIZE;

	return (sa & ~((uint64_t)AMC_SA_CHUNK_SIZE - 1)) | (mod + 1);
}

void amc_ctx_train(amc_ctx_t *ctx, uint64_t pc, uint64_t addr_line)
{
	bool tu_entry_found;
	training_unit_entry_t *tu_entry;
	int tu_entry_index;
	uint64_t last_addr_line;
	ps_amc_entry_t *ps_last_entry;
	ps_amc_entry_t *ps_cur_entry;
	training_unit_t *tu = &ctx->training_unit;
	ps_amc_t *ps = &ctx->ps;
	sp_amc_t *sp = &ctx->sp;

	debugf("AMC Train: pc = 0x%010lx, addr_line = 0x%010lx\n", pc, addr_line);

	/* Find the PC in the Training Unit */
	tu_entry_found = training_unit_find_pc(tu, pc, &tu_entry, &tu_entry_index);

	/* PC not found in the Training Unit: find a victim */
	if (!tu_entry_found) {
		training_unit_lru_get_victim(tu, &tu_entry, &tu_entry_index);
		/* Update the entry's data */
		tu_entry->pc = pc;
		tu_entry->last_addr_line = addr_line;
		tu_entry->valid = true;
	} else {
		last_addr_line = tu_entry->last_addr_line;
		/* Update the TU's last address */
		tu_entry->last_addr_line = addr_line;
	}

	/* Update the LRU info */
	training_unit_lru_access(tu, tu_entry_index);

	/* If we did not find the PC in the Training Unit or
	 * if we are accessing the same address, we are done */
	if (!tu_entry_found || last_addr_line == addr_line)
		return;

	/* Query entries for last and current access */
	ps_last_entry = ps_amc_query(ps, last_addr_line); // A
	ps_cur_entry = ps_amc_query(ps, addr_line);       // B

	debugf("  last: %p, cur: %p\n", ps_last_entry, ps_cur_entry);

	/* Both in the PS-AMC */
	// Both addresses are correlated. The same pc has accessed both, and they are different.
	if (ps_last_entry && ps_cur_entry) {
		debugf("Case Both\n");
		uint64_t last_sa_next = amc_ctx_sa_next(ps_last_entry->sa);
		/* Check if they are consecutive */
		if (ps_cur_entry->sa == last_sa_next) {
			/* Increment current's counter */
			if (ps_cur_entry->counter < PS_AMC_CONF_CNTR_MAX)
				ps_cur_entry->counter++;
		} else { /* Non-consecutive */ // Then, make them consecutive!
			/* Decrement current's counter */
			if (--ps_cur_entry->counter == 0) {
				ps_cur_entry->sa = last_sa_next;
				ps_cur_entry->counter = PS_AMC_CONF_CNTR_MAX;
				sp_amc_map(sp, ps_cur_entry->sa, addr_line);
			}
		}
	} else if (!ps_last_entry && !ps_cur_entry) { /* Neither in PS-AMC */
		debugf("Case Neither\n");
		uint64_t next_sa = amc_ctx_sa_alloc(ctx);
		ps_amc_map(ps, last_addr_line, next_sa);
		ps_amc_map(ps, addr_line, next_sa + 1);
		sp_amc_map(sp, next_sa, last_addr_line);
		sp_amc_map(sp, next_sa + 1, addr_line);
	} else if (ps_last_entry) { /* Only ps_last_entry */
		debugf("Case Only last\n");
		uint64_t next_sa = amc_ctx_sa_next(ps_last_entry->sa);
		ps_amc_map(ps, addr_line, next_sa);
		sp_amc_map(sp, next_sa, addr_line);
	} else { /* Only ps_cur_entry */
		debugf("Case Only current\n");
		uint64_t next_sa = amc_ctx_sa_alloc(ctx);
		ps_amc_map(ps, last_addr_line, next_sa);
		sp_amc_map(sp, next_sa, last_addr_line);
		/* Decrement current's counter */
		if (--ps_cur_entry->counter == 0) {
			ps_last_entry = ps_amc_query(ps, last_addr_line);
			ps_cur_entry->sa = amc_ctx_sa_next(ps_last_entry->sa);
			ps_cur_entry->counter = PS_AMC_CONF_CNTR_MAX;
			sp_amc_map(sp, ps_cur_entry->sa, addr_line);
		}
	}
}


int amc_ctx_predict(amc_ctx_t *ctx, uint64_t trigger_pa_line, uint64_t *prediction, int degree)
{
	ps_amc_t *ps = &ctx->ps;
	sp_amc_t *sp = &ctx->sp;
	ps_amc_entry_t *trigger_entry;
	int i, count;

	// Retrieve the trigger address' structural address
	trigger_entry = ps_amc_query(ps, trigger_pa_line);

	// If not found in the PS-AMC, we are done
	if (!trigger_entry || !trigger_entry->valid)
		return 0;

	// Predict the next consecutive structural addresses to prefetch
	count = 0;
	for (i = 0; i < degree; i++) {
		// For degree k prefetching, the prediction includes the next k structural addresses
		uint64_t sa = trigger_entry->sa + i + 1;
		// Query the SP-AMC for the next Structural Address
		sp_amc_entry_t *sp_entry = sp_amc_query(sp, sa);
		// If we don't find the Structural Address in the SP-AMC, continue
		if (!sp_entry || !sp_entry->valid)
			continue; // Continue or break?
		// Add the Physical Address to the prediction list
		prediction[count++] = sp_entry->pa_line;
	}

	return count;
}

/** Debugging functions **/

#ifdef INFO

void training_unit_print(const training_unit_t *tu)
{
	for (int i = 0; i < TRAINING_UNIT_NUM_STREAMS; i++) {
		const training_unit_entry_t *entry = &tu->entry[i];
		infof("{pc = 0x%010lx, last_addr_line = 0x%010lx, valid = %d}\n",
			entry->pc, entry->last_addr_line, entry->valid);
	}

	infof("\n");
}

void ps_amc_print(const ps_amc_t *ps)
{
	for (int i = 0; i < PS_AMC_NUM_SETS; i++) {
		infof("{");
		for (int j = 0; j < PS_AMC_NUM_WAYS; j++) {
			const ps_amc_entry_t *entry = &ps->set[i].way[j];
			infof("[%lx, %lx, %d, %d]",
				entry->tag, entry->sa, entry->counter, entry->valid);
		}
		infof("}\n");
	}

	infof("\n");
}

void sp_amc_print(const sp_amc_t *sp)
{
	for (int i = 0; i < SP_AMC_NUM_SETS; i++) {
		infof("{");
		for (int j = 0; j < SP_AMC_NUM_WAYS; j++) {
			const sp_amc_entry_t *entry = &sp->set[i].way[j];
			infof("[%lx, %lx, %d]",
				entry->tag, entry->pa_line, entry->valid);
		}
		infof("}\n");
	}

	infof("\n");
}

#endif
