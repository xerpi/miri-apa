tracePath="traces"
statsPath="stats"

function genIPCHearbeatCsv {
   inFile=$1   #gcc-hybridfull.out.txt
   outFile=$2  #datafile.csv

   grep "Instructions Retired" $inFile | sed "s/.*Cycle: \([0-9]\+\) .*IPC: \([0-9.]\+\) .*IPC: \([0-9.]\+\).*/\1\t\2\t\3/g" > $outFile
   echo "Created $outFile file."
}

function genInstLatencyCsv {
   inFile=$1
   outFile=$2

   grep "Instructions Retired" $inFile | awk '{print $3" "$5}' > $outFile
   echo "Created $outFile file."
}

function runprog {
   exe=$1
   traceFile=$2
   traceName=$3

   if [[ $# -eq 4 ]]; then
      conf=$4
   else
      conf=""
   fi

   if [[ "$conf" == "small_llc" ]]; then
      extraMake="CONF=-small_llc"
      extraName="-small_llc"
   elif [[ "$conf" == "low_bandwidth" ]]; then
      extraMake="CONF=-low_bandwidth"
      extraName="-low_bandwidth"
   elif [[ "$conf" == "scramble_loads" ]]; then
      extraMake="CONF=-scramble_loads"
      extraName="-scramble_loads"
   else
      extraMake=""
      extraName=""
   fi

   traceFullPath="$tracePath/$traceFile"
   outFile="$statsPath/$exe-$traceName$extraName.out"

   echo "make $exe"
   make $exe

   ext=$(echo $traceFile | awk -F'.' '{print $NF}')
   if [[ "$ext" == "gz" ]]; then
      echo "make run TRACE=$traceFullPath > $outFile"
      make run TRACE=$traceFullPath $extraMake > $outFile
   elif [[ "$ext" == "xz" ]]; then
      echo "make runxz TRACE=$traceFullPath > $outFile"
      make runxz TRACE=$traceFullPath $extraMake > $outFile
   else
      exit 1
   fi

   genIPCHearbeatCsv $statsPath/$exe-$traceName.out $statsPath/$exe-$traceName.csv
   genInstLatencyCsv $statsPath/$exe-$traceName.out $statsPath/$exe-$traceName-latency.csv
}

function genplot {
   colors=("green" "orange" "red" "blue" "magenta" "cyan" "yellow" "black")
   searchTerm=$1
   file=$2
   image=$3
   title=$4
   xlabel=$5
   ylabel=$6

   echo "set term png" > $file
   echo "set output \"$image\"" >> $file
   echo "set title \"IPC Comparison\"" >> $file
   #echo "set title \"$title\"" >> $file
   echo "set xlabel \"#cycles\"" >> $file
   #echo "set xlabel \"$xlabel\"" >> $file
   echo "set ylabel \"heartbeat IPC\"" >> $file
   #echo "set ylabel \"$ylabel\"" >> $file
   echo "set key left top" >> $file

   k=1
   for i in $(ls $searchTerm); do
      echo "set linestyle $k lc rgb \"${colors[$((k-1))]}\" lw 2" >> $file
      k=$((k+1))
   done

   k=1
   n=$(ls $searchTerm | wc -l)
   for i in $(ls $searchTerm); do
      aux=""
      if [[ $k -eq 1 ]]; then
         aux="plot "
      fi
      name=$(echo $i | awk -F '/' '{print $NF}' | awk -F'[-.]' '{print $1}')
      aux="$aux'$i' using 1:2 with lines ls $k title '$name', \\"
      if [[ $k -eq $n ]]; then
         aux=${aux:0:-3}
      fi
      echo "$aux" >> $file
      k=$((k+1))
   done
   echo "set term x11" >> $file

   echo "Created $file file."
}

function executeTraceWithAll {
   trace=$1
   traceName=$2
   plotName=$3

   if [[ $# -eq 4 ]]; then
      conf=$4
   else
      conf=""
   fi

   runprog hybridfull $trace $traceName $conf
   #runprog ampm $trace $traceName $conf
   #runprog isb4 $trace $traceName $conf
   #runprog id $trace $traceName $conf
   #runprog stream $trace $traceName $conf
   #runprog skeleton $trace $traceName $conf

   echo "genplot $statsPath/*-$traceName.csv plot.pt $plotName.png"
   genplot "$statsPath/*-$traceName.csv" "plot.pt" "$plotName.png" "IPC Comparison" "#cycles" "heartbeat IPC"
   gnuplot -p "plot.pt"

   #echo "genplot $statsPath/*-$traceName-latency.csv plot.pt $plotName-latency.png"
   #genplot "$statsPath/*-$traceName-latency.csv" "plot.pt" "$plotName-latency.png" "Latency Comparison" "Inst." "#cycles"
   #gnuplot -p "plot.pt"
}

# Integers:

# graphs
#executeTraceWithAll mcf_trace2.dpc.gz mcf mcf-comp
#executeTraceWithAll mcf_trace2.dpc.gz mcf mcf-comp-small_llc small_llc
#executeTraceWithAll mcf_trace2.dpc.gz mcf mcf-comp-low_bandwidth low_bandwidth
#executeTraceWithAll mcf_trace2.dpc.gz mcf mcf-comp-scramble_loads scramble_loads

# deepsjeng: pattern recognition, artificial intelligence
#executeTraceWithAll 631.deepsjeng_s-928B.champsimtrace.xz deepsjeng deepsjeng-comp
#executeTraceWithAll 631.deepsjeng_s-928B.champsimtrace.xz deepsjeng deepsjeng-comp-small_llc small_llc
#executeTraceWithAll 631.deepsjeng_s-928B.champsimtrace.xz deepsjeng deepsjeng-comp-low_bandwidth low_bandwidth
#executeTraceWithAll 631.deepsjeng_s-928B.champsimtrace.xz deepsjeng deepsjeng-comp-scramble_loads scramble_loads

# leela: Deep learning, with monte carlo simulationa and game tree search
#executeTraceWithAll 641.leela_s-334B.champsimtrace.xz leela leela-comp
#executeTraceWithAll 641.leela_s-334B.champsimtrace.xz leela leela-comp-small_llc small_llc
#executeTraceWithAll 641.leela_s-334B.champsimtrace.xz leela leela-comp-low_bandwidth low_bandwidth
#executeTraceWithAll 641.leela_s-334B.champsimtrace.xz leela leela-comp-scramble_loads scramble_loads

# graph500
executeTraceWithAll graph500-22.dpc.gz graph500 graph500-comp
executeTraceWithAll graph500-22.dpc.gz graph500 graph500-comp-small_llc small_llc
executeTraceWithAll graph500-22.dpc.gz graph500 graph500-comp-low_bandwidth low_bandwidth
executeTraceWithAll graph500-22.dpc.gz graph500 graph500-comp-scramble_loads scramble_loads

# FP:

# fotonik3d: Computational Electromagnetics
#executeTraceWithAll 649.fotonik3d_s-1B.champsimtrace.xz fotonik3d fotonik3d-comp
#executeTraceWithAll 649.fotonik3d_s-1B.champsimtrace.xz fotonik3d fotonik3d-comp-small_llc small_llc
#executeTraceWithAll 649.fotonik3d_s-1B.champsimtrace.xz fotonik3d fotonik3d-comp-low_bandwidth low_bandwidth
#executeTraceWithAll 649.fotonik3d_s-1B.champsimtrace.xz fotonik3d fotonik3d-comp-scramble_loads scramble_loads
