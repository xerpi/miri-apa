#!/bin/bash

function runtest {
   exe=$1
   trace=$2
   name=$3
   trUnit=$4
   ways=$5
   sets=$6

   echo "make $exe TRAINING=$trUnit WAYS=$ways SETS=$sets"
   make $exe TRAINING=$trUnit WAYS=$ways SETS=$sets
   echo "runplot TRACE=$trace"
   make runplot TRACE=$trace
   mv temporal.png $name-trUnit$trUnit-ways$ways-sets$sets-$exe.png
}

function runtests {
   exe=$1
   trUnit=$2
   ways=$3
   sets=$4

   runtest $exe traces/gcc_trace2.dpc.gz gcc $trUnit $ways $sets
   #runtest $exe traces/lbm_trace2.dpc.gz lbm $trUnit $ways $sets
   #runtest $exe traces/mcf_trace2.dpc.gz mcf $trUnit $ways $sets
   #runtest $exe traces/leslie3d_trace2.dpc.gz leslie3d $trUnit $ways $sets
   #runtest $exe traces/milc_trace2.dpc.gz milc $trUnit $ways $sets
   #runtest $exe traces/GemsFDTD_trace2.dpc.gz gems $trUnit $ways $sets
   #runtest $exe traces/libquantum_trace2.dpc.gz libquantum $trUnit $ways $sets
   #runtest $exe traces/omnetpp_trace2.dpc.gz omnetpp $trUnit $ways $sets
}

function checkChanges {
   trUnit=$1
   ways=$2
   sets=$3

   #runtests isb $trUnit $ways $sets
   runtests isb2 $trUnit $ways $sets
}

checkChanges 8 8 8
checkChanges 8 16 8
checkChanges 8 8 16
checkChanges 8 16 16
checkChanges 32 8 8
checkChanges 32 16 8
checkChanges 32 8 16
checkChanges 32 16 16
