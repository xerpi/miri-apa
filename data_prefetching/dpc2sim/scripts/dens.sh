#!/bin/bash

function runprog {
   exe=$1
   trace=$2
   name=$3
   trUnit=$4
   ways=$5
   sets=$6
   density=$7

   echo "#runprog exe=$exe trace=$trace name=$name trUnit=$trUnit ways=$ways sets=$sets density=$density"

   if [[ $exe == "hybrid" ]]; then
      echo "make $exe TRAINING=$trUnit WAYS=$ways SETS=$sets DENSITY=$density"
      make $exe TRAINING=$trUnit WAYS=$ways SETS=$sets DENSITY=$density
   else
      echo "make $exe"
      make $exe
   fi
   echo "runplot TRACE=$trace"
   make runplot TRACE=$trace
   if [[ $exe == "hybrid" ]]; then
      mv datafile.csv $name-trUnit$trUnit-ways$ways-sets$sets-$exe+$density.csv
      mv temporal.png $name-trUnit$trUnit-ways$ways-sets$sets-$exe+$density.png
   else
      mv datafile.csv $name-trUnit$trUnit-ways$ways-sets$sets-$exe.csv
      mv temporal.png $name-trUnit$trUnit-ways$ways-sets$sets-$exe.png
   fi
   mv out.txt $name-$exe.out.txt
}

function runprogs {
   trace=$1
   tracename=$2
   trUnit=$3
   ways=$4
   sets=$5

   echo "#runprogs trace=$trace tracename=$tracename trUnit=$trUnit ways=$ways sets=$sets"

   runprog ampm $trace $tracename $trUnit $ways $sets
   for i in $(seq 5 5 20); do
      runprog hybrid $trace $tracename $trUnit $ways $sets $i
   done
}

function genplot {
   colors=("green" "orange" "red" "blue" "magenta" "cyan" "yellow" "black" "pink" "grey" "green")
   file=$1
   image=$2

   echo "set term png" > $file
   echo "set output \"$image\"" >> $file
   echo "set title \"IPC Comparison\"" >> $file
   echo "set xlabel \"# of instructions\"" >> $file
   echo "set ylabel \"IPC\"" >> $file

   k=1
   for i in $(ls *.csv); do
      echo "set linestyle $k lc rgb \"${colors[$((k-1))]}\" lw 2" >> $file
      k=$((k+1))
   done

   k=1
   n=$(ls *.csv | wc -l)
   for i in $(ls *.csv); do
      aux=""
      if [[ $k -eq 1 ]]; then
         aux="plot "
      fi
      name=$(echo $i | awk -F '-' '{print $NF}' | awk -F '.' '{print $1}')
      aux="$aux'$i' using 1:2 with lines ls $k title '$name', \\"
      if [[ $k -eq $n ]]; then
         aux=${aux:0:-3}
      fi
      echo "$aux" >> $file
      k=$((k+1))
   done
   echo "set term x11" >> $file
}

function compareTrace {
	trace=$1
	name=$2
	trUnit=$3
	ways=$4
	sets=$5

	echo "#compareTrace trace=$trace name=$name trUnit=$trUnit ways=$ways sets=$sets"

	rm *.csv
	runprogs traces/$trace $name $trUnit $ways $sets
	genplot "plot.pt" "comp-$name.png"
	gnuplot -p "plot.pt"
}

#compareTrace gcc_trace2.dpc.gz gcc 8 8 8
compareTrace GemsFDTD_trace2.dpc.gz gems 8 8 8
#compareTrace leslie3d_trace2.dpc.gz leslie3d 8 8 8
compareTrace mcf_trace2.dpc.gz mcf 8 8 8
compareTrace lbm_trace2.dpc.gz lbm 8 8 8
compareTrace omnetpp_trace2.dpc.gz omnetpp 8 8 8
compareTrace libquantum_trace2.dpc.gz libquantum 8 8 8
compareTrace milc_trace2.dpc.gz milc 8 8 8
