#!/bin/bash

# Copied from: https://github.com/jsmont/data_prefetcher/blob/master/scripts/plot_trace.sh

REPOROOT=$(git rev-parse --show-toplevel)
DATAFILE=datafile.csv

grep "Instructions Retired" $1 > $DATAFILE

sed -i "s/.*Cycle: \([0-9]\+\) .*IPC: \([0-9.]\+\) .*IPC: \([0-9.]\+\).*/\1\t\2\t\3/g" $DATAFILE

gnuplot -p "$REPOROOT/Data prefetching/dpc2sim/scripts/temporal.pt"
